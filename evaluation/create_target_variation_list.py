import pandas as pd
import glob
import os

if os.path.isfile("stories.py"):
    from stories import stories
else:
    print("Copy stories.py from data collection app to this directory.")
    exit()


df_stories = (
    pd.DataFrame().from_dict(stories).drop(["id", "path", "type"], axis=1)
)  # to match "title" column in audio files to "quote" column in output.csv

df_errors = pd.DataFrame()

approved_errors_folder = "../synthetic_data_generation/phonological_errors/approved"

for error_csv in glob.glob(approved_errors_folder + "/*.csv"):
    df_er = pd.read_csv(error_csv)
    df_errors = pd.concat([df_errors, df_er], axis=0)

df = pd.merge(df_errors, df_stories, left_on="Variation", right_on="quote", how="inner")
df = df.drop_duplicates("Variation")

var_list = []
for target in df["Correct"].unique():
    df_vars = df[df["Correct"] == target]
    var_list += list(df_vars["Variation"])

var_list = var_list + list(df_errors.Correct.unique())  # now add the correct words
print(len(var_list))
var_list=list(set(var_list))
print(len(var_list))

with open("target_variations.txt", "w") as f:
    for word in var_list:
        f.write(word + "\n")
