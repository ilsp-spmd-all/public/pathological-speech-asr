#!/usr/bin/env bash
cmd(){ echo `basename $0`; }
usage(){
    echo "\
    `cmd` [OPTION...]
    -i, --input; Input wav folder
    -o, --output; Output resampled folder
    -r, --sample-rate; Target sample rate of audio
    -j, --njobs; Number of cores for parallel execution [default: `grep -c ^processor /proc/cpuinfo`]
    " | column -t -s ";"
}

print_usage(){
    usage;
    exit 2;
}

abnormal_exit(){
    usage;
    exit 1;
}

SHORT_OPTS=i:o:r:j:
LONG_OPTS=input:,output:,sample-rate:,njobs:

OPTIONS=`getopt -o ${SHORT_OPTS} --long ${LONG_OPTS} -n "resample_folder.sh" -- "$@"`

if [ $? != 0 ] ; then abnormal_exit; fi

INPUT_FOLDER=
OUTPUT_FOLDER=
TARGET_SR=
NJOBS=`grep -c ^processor /proc/cpuinfo`

while true; do
  case "$1" in
    -i | --input ) INPUT_FOLDER="$2"; shift 2 ;;
    -o | --output ) OUTPUT_FOLDER="$2"; shift 2 ;;
    -j | --njobs ) NJOBS="$2"; shift 2 ;;    
    -r | --sample-rate ) TARGET_SR="$2"; shift 2 ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

echo INPUT_FOLDER $INPUT_FOLDER
echo OUTPUT_FOLDER $OUTPUT_FOLDER
echo NJOBS $NJOBS
echo TARGET_SR $TARGET_SR


if [ -z "$INPUT_FOLDER" ]
then
    abnormal_exit
fi

if [ -z "$OUTPUT_FOLDER" ]
then
    abnormal_exit
fi

resample_wav() {
    wav=$1
    outf=$2
    target_sr=$3
    base_wav=${wav%.*}
    base_wav=${base_wav##*/}
    # kaldigrpc-transcribe --host $host --port $port --streaming $wav > ${outf}/${base_wav}.txt
    sox $wav ${outf}/${base_wav}.wav rate $target_sr
}

export -f resample_wav

parallel -j${NJOBS}  resample_wav {} $OUTPUT_FOLDER $TARGET_SR ::: $(find ${INPUT_FOLDER} -name "*.wav")
