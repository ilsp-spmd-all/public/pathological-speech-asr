import './App.css';
import MyCard from './components/UI/MyCard'
import RecordingHandler from './components/Exercises/Audio/RecordingHandler'
import { Router, Link } from "@reach/router";

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <nav>
         <h3><Link to="rabbit">«Λαγός» - </Link> <Link to="turtle"> «Χελώνα» </Link> </h3>
      </nav>
      <Router>
        <Rabbit path="rabbit"/>
        <Turtle path="turtle"/>
      </Router>
      </header>
    </div>
  );
}
const Turtle= () =>(
    <>
      <MyCard>
      <img src="turtle.jpg" alt="a turtle" style={{width: 400}}></img>
      </MyCard>
      <MyCard>
        <RecordingHandler target="χελωνα"/>
      </MyCard>
        <h3> Πιθανά λάθη </h3>
        <ul>
          <li>κελώνα</li>
          <li>χελώλα</li>
          <li>χιουλώνα</li>
          <li>δεδώνα</li>
          <li>ρελώνα</li>
          <li>τερώνα</li>
          <li>χιλώνα</li>
          <li>κελώλα</li>
        </ul>
    </>
)
const Rabbit= () =>(
    <>
      <MyCard>
      <img src="rabbit.jpg" alt="a rabbit" style={{width: 400}}></img>
      </MyCard>
      <MyCard>
        <RecordingHandler target="λαγος"/>
      </MyCard>
        <h3> Πιθανά λάθη </h3>
      <ul>
        <li>γαλός</li>
        <li>γαγός</li>
        <li>τάρκος</li>
        <li>λακός</li>
        <li>νταρκός</li>
      </ul>
    </>
)

export default App;
