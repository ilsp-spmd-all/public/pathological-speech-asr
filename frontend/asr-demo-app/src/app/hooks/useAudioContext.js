import {useEffect, useState} from "react";
import {_arrayBufferToBase64, convertFloatToByteArray, downSample} from "../helpers/audioManipulation";

const useAudioContext = (stream, wsSendChunks, ws) => {

    const [audioCtx, setAudioCtx] = useState(null);
    const [processor, setProcessor] = useState()


    useEffect(() => {
        const AudioContext = window.AudioContext || window.webkitAudioContext;
        const context = new AudioContext({
            // latencyHint: 'interactive',
            // sampleRate: 16000,//16000,
        });
        const processor = context.createScriptProcessor(8 * 1024, 1, 1);// 4* 1024

        setProcessor(processor)
        setAudioCtx(context);

        return () => {
            context.close()
        }
    }, [])

    useEffect(() => {
            if (audioCtx) {
                if (!stream) {
                    audioCtx.suspend()
                } else {
                    audioCtx.resume();
                    // console.log(audioCtx)

                    const source = audioCtx.createMediaStreamSource(stream);
                    source.connect(processor);

                    processor.onaudioprocess = (e) => {
                        // Do something with the data, e.g. convert it

                        const inputData = e.inputBuffer.getChannelData(0);
                        //  Resample inputData
                        const resampledAudio = downSample(inputData, audioCtx.sampleRate, 16000);
                        // console.log({resampledAudio})
                        const ab = convertFloatToByteArray(resampledAudio);
                        const base64Data = _arrayBufferToBase64(ab);

                        let dataToSend = {
                            "is_start": false,
                            "is_chunk": true,
                            "target": "",
                            "base64_chunk": base64Data
                        }
                        // **  Send it through webSocket? **
                        wsSendChunks(ws, dataToSend)

                        // console.log({
                        //     dataToSend, timestamp: Date.now()
                        // })
                    };
                    processor.connect(audioCtx.destination);
                }
            }

        }, [stream, wsSendChunks, ws, audioCtx, processor]
    )
    ;

// return [audioCtx,audioContextHandleStream];
    return [audioCtx];
}
export default useAudioContext;
