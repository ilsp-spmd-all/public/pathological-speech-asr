import { useEffect, useState} from "react";

/*
correctness_threshold: 50
error_message: ""
is_error: false
is_single_utterance_end: false
recognition_result: "γάτα"
score: 100
status: 0
 */
const useWebSocket = (url) => {
    const [ws, setWs] = useState(null);
    const [isOpen, setIsOpen] = useState(false);
    const [isConnecting, setIsConnecting] = useState(false);

    const wsIsActive= isOpen||isConnecting;

    // set isOpen
    useEffect(() => {
        if (ws && ws.readyState === 1) {
            setIsOpen(true)
        }
        if (ws && (ws.readyState === 2 || ws.readyState === 3)) {
            setIsOpen(false)
        }
    }, [{ws}])

    const wsConnect = (cbStartStream,cbOnClose,cbHandleResponse) => {
        setIsConnecting(true);
        const newWs = new WebSocket(url);

        newWs.onopen = (e) => {
            setIsConnecting(false);
            setWs(newWs)
            console.log("WS Connected!")
            setIsOpen(true)
            cbStartStream(newWs);
            // ws.send(JSON.stringify(apiCall));
            // ws.send(JSON.stringify({'exercise_id': 'asd'}))
            // ws.send(JSON.stringify({'q_id': '23'}))
        };
        newWs.onmessage = (e) => {
            const json = JSON.parse(e.data);
            console.log(json)
            try {
                if ((json.recognition_result)) {
                    if (cbHandleResponse) {
                        cbHandleResponse(json);
                    }
                    console.log(json.recognition_result)
                }
                if ((json.recognition_result==='γάτα')) {
                    newWs.close()
                }

            } catch (err) {
                console.log(err);
            }
        };
        newWs.onclose = (e) => {
            // console.log(e)
            cbOnClose();    //  stop sending data (chunks) onClose
            console.log("WS Disconnected!")
        };
        newWs.onerror = (e) => {
            console.log('EEError: ', e)
            setIsConnecting(false);
        }
    };
    const wsClose = () => {
        if (ws) {
            ws.close();
        }
    }

    useEffect(() => {
        //cleanUp
        return () => {
            if (ws) {
                console.log('cleanup ws')
                ws.close();
            }
        }
    }, [ws]);

    return [ws,wsIsActive, wsConnect,wsClose];
}
export default useWebSocket;