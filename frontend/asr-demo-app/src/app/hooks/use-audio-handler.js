import React, {useEffect, useState} from 'react';
import useWebSocket from "./use-webSocket";
import useAudioContext from "./useAudioContext";
import useMediaRecorder from "./use-mediaRecorder";
const BACKEND_URL='wss://dialogue.ilsp.gr:8897/audio-evaluation-ws'
const wsInitializeExercise = (ws,target) => {
    if (ws && ws.readyState === 1) {
        const firstDataToSend = {"is_start": true, "is_chunk": false, "target": target, "base64_chunk": ""}
        ws.send(JSON.stringify(firstDataToSend));
        console.log(firstDataToSend)
    }
}
const wsSendChunks = (ws, dataObj) => {
    // if (ws && ws.readyState === 1) {
    ws.send(JSON.stringify(dataObj));
    // }
    // console.log({ws: ws.readyState, dataSent: dataObj})
}
const useAudioHandler = (onCloseComplete, checkRecordingResponseHandler, onGetRecordedBlob, exerciseDataToInitializeConnection) => {

    const target=exerciseDataToInitializeConnection
    console.log(target)
    const [isRecording, setIsRecording] = useState(false);
    const [isLoading, setIsLoading] = useState(false)
    const [audioStream, setAudioStream] = useState(null);


    // console.log('UseAudioHandler: ',{isMicVisible, onCloseComplete, onRecord})
    // console.log('UseAudioHandler: ',{ onGetRecordedBlob,audioStream,mRecorder})
    // const [ws,wsIsConnecting,wsIsOpen, wsNewConnection,wsClose] = useWebSocket('ws://localhost:9001/audio-evaluation-ws');
    const [ws, wsIsActive, wsNewConnection, wsClose] = useWebSocket(BACKEND_URL);

    // Both depend on stream for (resume/start) / (suspend/stop)
    useAudioContext(audioStream, wsSendChunks, ws);
    useMediaRecorder(audioStream, onGetRecordedBlob);

    const onWsConnectionStartRec = () => {
        // setIsLoading(false);
        setIsRecording(true);
    }
    const startConnection = () => {
        setIsLoading(true);
        wsNewConnection(onWsConnectionStartRec, wsClosingHandler, checkRecordingResponseHandler);
    }
    useEffect(() => {
        console.log({isRecording, audioStream})
        if (isRecording && audioStream) {
            setIsLoading(false);
        }
        if (isRecording && !audioStream) {
            wsInitializeExercise(ws,target);
            startStreaming()
        }
        if (!isRecording && audioStream) {
            setAudioStream(null)
            // stopStreaming();
        }
        if (!isRecording && !audioStream) { // && ws) {
            // wsClose();
        }
    }, [isRecording, audioStream])//stream
    useEffect(() => {
        return () => {
            if (audioStream) {
                stopStreaming();
            }
        }
    }, [audioStream])

    const startStreaming = async () => {
        // Start Stream
        const stream = await navigator.mediaDevices.getUserMedia({audio: true, video: false})//{audio: {sampleSize: 16, channelCount: 2},video: true}
        setAudioStream(stream);
        // console.log('startStreaming: ', {stream})
    }
    const stopStreaming = () => {
        console.log('Stop Streaming... ')
        audioStream.getAudioTracks().forEach(track => track.stop());
        setAudioStream(null);
    }
    const wsClosingHandler = () => {
        stopRecording();
        setIsLoading(false)
        onCloseComplete();
    }

    const stopRecording = () => {
        console.log('Stop Recording... ')
        setIsRecording(false);
        setIsLoading(true)
        //onCloseComplete();
    }

    return {
        audioStream,
        startConnection,
        stopRecording,
        wsIsActive,
        isLoading
    };
}

export default useAudioHandler;
