import { useEffect, useState} from "react";

const useMediaRecorder = (audioStream, onGetRecordedBlob) => {

    const [mRecorder, setMRecorder] = useState(null);
    let chunks = [];

    useEffect(() => {
        // Handle MediaRecorder
        let mediaRecorder
        if (audioStream) {
            mediaRecorder = new MediaRecorder(audioStream);
            setMRecorder(mediaRecorder);
            mediaRecorder.start(4500);
            mediaRecorder.ondataavailable = function (e) {
                chunks.push(e.data);
                console.log("chunks pushed", e.data)
            }
            mediaRecorder.onstop = function (e) {
                const blobListen = new Blob(chunks, {});
                // console.log("audioElement: ", audioElement)
                // audioElement.src = URL.createObjectURL(blobListen);
                if(onGetRecordedBlob){
                    onGetRecordedBlob(blobListen);
                }
                chunks = [];
                setMRecorder(null);
            }
        }
        return () => {
            //cleanup mRecorder
            if (mediaRecorder && mediaRecorder.state === 'recording') {
                mediaRecorder.stop();
            }
            //cleanup mediaDevices freeMicrophone
            if (audioStream) {
                // console.log("free Device: ", {audioStream})
                // audioStream.getAudioTracks().forEach(track => track.stop());
            }
        }

    }, [audioStream])

    return [mRecorder];
}
export default useMediaRecorder;