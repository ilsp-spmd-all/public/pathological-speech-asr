export function convertFloatToByteArray(data) {
    // data is a Float32Array
    let buffer = new ArrayBuffer(data.length * 2);
    let view = new DataView(buffer);
    let index = 0;
    let volume = 1;
    for (let i = 0; i < data.length; i++) {
        view.setInt16(index, data[i] * (0x7FFF * volume), true);
        index += 2;
    }
    return view.buffer;
}

export function _arrayBufferToBase64(buffer) {
    let binary = '';
    let bytes = new Uint8Array(buffer);
    let len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
}

export function downSample(buffer, fromSampleRate, toSampleRate) {
    // buffer is a Float32Array
    const sampleRateRatio = Math.round(fromSampleRate / toSampleRate);
    const newLength = Math.round(buffer.length / sampleRateRatio);

    const result = new Float32Array(newLength);
    let offsetResult = 0;
    let offsetBuffer = 0;
    while (offsetResult < result.length) {
        const nextOffsetBuffer = Math.round((offsetResult + 1) * sampleRateRatio);
        let accum = 0;
        let count = 0;
        for (let i = offsetBuffer; i < nextOffsetBuffer && i < buffer.length; i++) {
            accum += buffer[i];
            count++;
        }
        result[offsetResult] = accum / count;
        offsetResult++;
        offsetBuffer = nextOffsetBuffer;
    }
    return result;
}