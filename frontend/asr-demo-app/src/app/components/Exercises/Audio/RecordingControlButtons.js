import React from 'react';
import classes from "./RecordingControlButtons.module.css";
import BootstrapIconButton from "../../UI/BootstrapIconButton";
import MyCard from "../../UI/MyCard";


const RecordingControlButtons = props => {
    const {playingSound=false} = props


    return (
        <MyCard className={classes['control-buttons']}>

            <BootstrapIconButton className={`btn-dark ${playingSound?classes['playing-sound']:''}`}
                                 onClick={props.onPlayRecording}
                                 text={playingSound?'Ακούτε τον ήχο':'Ακούστε τον ήχο'}
                                 icon={playingSound?'fas fa-stop':'fas fa-play'}
                                 smallScreenHideText={true}
            />
            {props.showTryAgainButton &&
            <BootstrapIconButton className="btn-warning "
                                 onClick={props.onTryAgain}
                                 text='Προσπαθήστε Ξανά'
                                 icon='fas fa-microphone'
                                 smallScreenHideText={true}
            />}

        </MyCard>
    );
};

export default RecordingControlButtons;
