import React, {useCallback, useRef, useState} from 'react';
import useAudioHandler from "../../../hooks/use-audio-handler";
import RecordingControlButtons from "./RecordingControlButtons";
import AudioAnalyser from "./AudioAnalyser";
import UnderMicLoadingSpinner from "../../UI/Spinners/UnderMicLoadingSpinner";
import MyCard from "../../UI/MyCard";
import AudioButton from "./AudioButton";


function ResultBox(props) {
    return (
        <MyCard>
        <button> {props.value} </button>
        </MyCard>
    )}
    


const RecordingHandler = props => {
    const target=props.target
    console.log(target)

    const [hasRecorded, setHasRecorded] = useState(false);
    const [playingSound, setPlayingSound] = useState(false);
    const [last_asr_result, setLastAsrResult] = useState(false);
    // const [showMic, setShowMic] = useState(true);
    // const [saveButtonVisible, setSaveButtonVisible] = useState(false);
    const [tryAgainButtonVisible, setTryAgainButtonVisible] = useState(true);

    const audioRef = useRef(null);
    const audioElement = audioRef.current;


    const showRecordButtons = () => {
        setHasRecorded(true)
    }
    // const showMicHandler = () => {
    //     setShowMic(true)
    // }
    // const hideMicHandler = () => {
    //     setShowMic(false)
    // }
    const getRecordedAudioHandler = useCallback((blob) => {
        // audioElement.src = URL.createObjectURL(blob); problematic
        // if (audioElement) {
        if (audioRef && audioRef.current) {
            audioRef.current.src = URL.createObjectURL(blob);
        }
    }, [])
    const playRecordingHandler = () => {
        if (!audioElement) return;
        if(!playingSound){
            audioElement.play()
            setPlayingSound(true)
        }else{
            audioElement.pause()
            audioElement.currentTime = 0;
            setPlayingSound(false)
        }
    }
    const soundFinishedHandler = () => {
        setPlayingSound(false)
        console.log('asfasf');
    }

    const checkRecordedResult = useCallback((resultJson) => {
        console.log("pee")
        // console.log({recRes:resultJson.recognition_result, sc:resultJson.score})
        if (resultJson.score === 100 && resultJson.recognition_result === 'γάτα') {
            //setTryAgainButtonVisible(false);
            //dispatch(actions.changeAvatar('/video/2'));
            // setSaveButtonVisible(true);
            setLastAsrResult(resultJson.recognition_result+", score: "+resultJson.score.toString())
            console.log("χαχαχα")
        }
        setLastAsrResult(resultJson.recognition_result+", score: "+resultJson.score.toString())
        //onRecordCompletedOnce();
        setHasRecorded(true);
    }, [])

    //  showMicrophone
    const recordAgain = () => {
        setHasRecorded(false)
    }

    const {audioStream,startConnection,stopRecording,wsIsActive,isLoading} = useAudioHandler(showRecordButtons,checkRecordedResult,getRecordedAudioHandler,target);
    return (<>
            <div hidden={hasRecorded}>
                <MyCard>
                    <AudioButton audioStream={audioStream}
                                 startConnection={startConnection}
                                 stopRecording={stopRecording}
                                 wsIsActive={wsIsActive}
                                 isLoading={isLoading}
                    />
                </MyCard>
            </div>
            {hasRecorded &&
            <RecordingControlButtons
                onTryAgain={recordAgain}
                onPlayRecording={playRecordingHandler}
                // showNextButton={saveButtonVisible}
                showTryAgainButton={tryAgainButtonVisible}
                playingSound={playingSound}
            />}
            <audio ref={audioRef} controls hidden
                   onEnded={soundFinishedHandler}
                   // src={toAbsoluteUrl("/file_example.mp3")}
            />
            <div>
            <ResultBox value={last_asr_result} />
            </div>
        </>
    );
};

export default RecordingHandler;
