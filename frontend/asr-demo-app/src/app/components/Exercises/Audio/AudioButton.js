import React, {useEffect, useState} from 'react';
import AudioAnalyser from './AudioAnalyser';
import MyCard from "../../UI/MyCard";
import UnderMicLoadingSpinner from "../../UI/Spinners/UnderMicLoadingSpinner";
import classes from "./AudioButton.module.css";
import BootstrapIconButton from "../../UI/BootstrapIconButton";


const AudioButton = (props) => {

    const {audioStream, startConnection, stopRecording, wsIsActive, isLoading,singleIconButton=false} = props;

        const clickHandler = (event) => {
            event.stopPropagation();
        }
    return (<>
                <div onClick={clickHandler}>
                    <BootstrapIconButton className={`btn-warning `}
                                         onClick={startConnection}
                                         text={!singleIconButton?'Ηχογράφηση':''}
                                         icon={'fas fa-microphone-slash'}
                                         disabled={wsIsActive || audioStream}
                                         smallScreenHideText={true}
                    />
                </div>

            {(audioStream || isLoading) && <>
                <MyCard className={classes.modal}>
                    {audioStream && <>
                        <BootstrapIconButton className={`btn-warning `}
                                             onClick={stopRecording}
                                             text={'Κλείσιμο'}
                                             icon={'fas fa-microphone'}
                                             disabled={false}
                        />

                        <AudioAnalyser audio={audioStream}/>
                    </>}

                    {isLoading && <UnderMicLoadingSpinner/>}

                </MyCard>
                <div className={classes.backdrop}/>
            </>
            }
        </>
    );
}

export default AudioButton;
