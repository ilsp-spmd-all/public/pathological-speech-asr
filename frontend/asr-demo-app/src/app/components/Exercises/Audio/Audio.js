import React, {useEffect, useState} from 'react';
import AudioAnalyser from './AudioAnalyser';
import MyCard from "../../UI/MyCard";
import UnderMicLoadingSpinner from "../../UI/Spinners/UnderMicLoadingSpinner";

const AudioBtn = (props) => {
    const [audioStream, setAudioStream] = useState(null);

    // useEffect(() => {
    //     getMicrophone();
    //     // props.onRecord();
    //     console.log("getMicrophone")
    //     return () => {
    //         console.log("stopMicrophone: on unmount")
    //         stopMicrophone();
    //         // props.onCloseComplete();
    //     }
    // }, [])

    useEffect(() => {
        if (props.isOn) {
            getMicrophone();
        } else {
            if (audioStream) {
                stopMicrophone();
            }
        }
    }, [props.isOn])

    const getMicrophone = async () => {
        const stream = await navigator.mediaDevices.getUserMedia({
            audio: true,
            video: false
        });
        console.log("getMicrophone: ", stream)
        setAudioStream(stream);
    }

    const stopMicrophone = () => {
        audioStream.getTracks().forEach(track => track.stop());
        setAudioStream(null);
    }

    return (<>
            {props.isOn &&

            <div>
                {/*<MyCard className={'mt-3'}>*/}
                {/*    <div style={{marginTop: 4}}>*/}
                {/*        {audioStream &&*/}
                {/*        <button className='btn btn-sm btn-warning' onClick={stopRecording} disabled={false}>*/}
                {/*            <i className="fas fa-microphone"/>*/}
                {/*        </button>}*/}
                {/*        {!audioStream && !(wsIsActive) &&*/}
                {/*        <button className='btn btn-sm btn-warning' onClick={startConnection}*/}
                {/*                disabled={wsIsActive}>*/}
                {/*            <i className="fas fa-microphone-slash"/>*/}
                {/*        </button>}*/}
                {/*        {isLoading && <UnderMicLoadingSpinner/>}*/}
                {/*        /!*{audioStream ? '  Γίνεται εγγραφή' : '  πάτα εδώ για ηχογραφηση'}*!/*/}
                {/*    </div>*/}

                    <div>
                        {audioStream ? <AudioAnalyser audio={audioStream}/> : ''}
                    </div>
                {/*</MyCard>*/}
             </div>

            }
        </>
    );
}

export default AudioBtn;