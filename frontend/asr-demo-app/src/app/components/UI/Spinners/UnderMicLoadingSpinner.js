import React from "react";
import classes from './UnderMicLoadingSpinner.module.css';

const UnderMicLoadingSpinner = (props) => {

    return <>
        {/*<div className={classes.overlay}>*/}
        {/*    <div className={classes['continuous-bar']}/>*/}
        {/*</div>*/}
        <div className={classes.overlay}>
            <div className={classes['spinner-2']} />
        </div>
    </>;
}

export default UnderMicLoadingSpinner;

// const{size='md'}=props;
//
// let myClass;
//
// switch (size){
//   case 'sm':
//     myClass= `${classes.spinner} ${classes['spinner-sm']}`
//     break;
//   case 'lg':
//     myClass= `${classes.spinner} ${classes['spinner-lg']}`
//     break;
//   default:
//     myClass= `${classes.spinner} ${classes['spinner-md']}`
//     break;
// }