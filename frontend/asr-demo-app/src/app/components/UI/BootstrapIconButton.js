import React from 'react';
import classes from "./BootstrapIconButton.module.css";


const BootstrapIconButton = props => {

    const bootstrapBtn = props.isDisabled ? `btn-secondary ` : props.className;
    const btnClass = props.isDisabled? `${classes.button} ${classes.disabled}`:classes.button;
const smallScreenHideTextClass = props.smallScreenHideText ?  classes['hide-text']:`` ;
    console.log(props.smallScreenHideText)
    return (
        <button className={`btn ${bootstrapBtn} ${props.otherClasses} ${btnClass} `}
                onClick={props.onClick}
                title={props.title}
                disabled={props.isDisabled}
        >

            {props.icon && <i className={`${props.icon} ${classes.icon}`}/>}

            {props.text && <span className={`${classes.text} ${smallScreenHideTextClass}`}> {props.text} </span>}

        </button>
    );
};

export default BootstrapIconButton;
