# ASR demo app

Simple react app that streams audio back to the asr backend.

## Instructions

install dependencies:

```
npm install
```

serve on localhost:3000

```
npm start
```
