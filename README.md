# Speech Pathology Detection

This repo contains the code for building and evaluating a pathological speech evaluation app. The backend is an asr server supporting streaming ASR with Google's ASR and Kaldi-grpc, as well as offline ASR with OpenAI's Whisper. The frontend is a simple interface for evaluating single word utterances.

## Installation

### Prerequisites

#### Frontend Environment
* npm
* React
#### Backend Environment
* docker 
* docker-compose 
* server with a TLS certificate


For the frontend, install the required react packages by running
```
cd frontend/asr-demo-app/ && npm install
```
Regarding the installation of the frontend, no package installation is required.

## Configuration

### Backend
You need to deploy the backend on server that accepts HTTP connections, that also 
has a TLS key-cert pair. For more info, refer to the [README of the backend repo](https://gitlab.com/ilsp-spmd-all/dialogue/generic-voice-assistant/-/blob/planv/README.md)

### Frontend

Set the correct host and port to connect to the backend websocket, by editing the 
component `frontend/asr-demo-app/src/app/hooks/use-audio-handler.js` and setting the backend url constant, defined in the first lines, to `wss://HOST:PORT/audio-evaluation-ws`.

## Start the app

### Backend
```
docker-compose up
```

### Frontend 
```
HTTPS=true npm start
```

## ASR demo

