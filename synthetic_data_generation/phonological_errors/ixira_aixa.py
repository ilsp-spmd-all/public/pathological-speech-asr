import json
import csv
from utils import (
    swap_string_all_combinations,
    swap_string_all_combinations_no_duplicates,
)

with open("word_to_syllable.json", "r") as f:
    word2syl = json.load(f)

rules_abbr2full = {
    "ix_f_b": "Ηχηροποίηση φ - β",
    "ix_t_nt": "Ηχηροποίηση τ - ντ",
    "ix_u_d": "Ηχηροποίηση θ - δ",
    "ix_s_z": "Ηχηροποίηση σ - ζ",
    "ix_ki_gki": "Ηχηροποίηση κι - γκι",
    "ix_ka_gka": "Ηχηροποίηση κα - γκα",
    "ix_xi_gi": "Ηχηροποίηση χι - γι",
    "ix_xa_ga": "Ηχηροποίηση χα - γα",
}

rules_subst = {
    "ix_f_b": {"φ":"β"},
    "ix_t_nt": {"τ":"ντ"},
    "ix_u_d": {"θ":"δ"},
    "ix_s_z": {"σ":"ζ"},
    "ix_ki_gki": {"κι":"γκι"},
    "ix_ka_gka": {"κα":"γκα"},
    "ix_xi_gi": {"χι":"γι"},
    "ix_xa_ga": {"χα":"γα"},
}

greek_consontants_regex = "β|γ|δ|ζ|θ|κ|λ|μ|ν|ξ|π|ρ|σ|τ|φ|χ|ψ"
greek_vowels_unusual = ["αη", "αϊ", "οη", "οϊ"]
greek_vowels_double = ["αι", "ει", "οι", "ου", "υι", "αυ", "ειυ", "ια"]
greek_vowels_single = ["α", "ε", "η", "ι", "υ", "ο", "ω"]


def string_sub_rule(word, rule):
    if rule not in rules_subst:
        print("No such rule!")
    else:
        mistakes_list = []
        for substitution in rules_subst[rule]:
            to_swap = substitution
            print(to_swap)
            swap_with = rules_subst[rule][substitution]
            print(swap_with)
            new_words = swap_string_all_combinations_no_duplicates(
                word, to_swap, swap_with
            )
            mistakes_list = mistakes_list + new_words
    print(mistakes_list)
    return mistakes_list


def apply_all_rules(word):
    csv_lines = []
    for rule in rules_abbr2full.keys():
        rule_full = rules_abbr2full[rule]
        try:
            mistakes_list = string_sub_rule(word, rule)
        except NotImplementedError:
            print("WARNING!: Rule " + rule_full + " not implemented yet!")
            continue

        csv_lines += [[word, x, rule_full] for x in mistakes_list]
    return csv_lines


def main():

    csv_headers = ["Correct", "Variation", "Error Type"]
    csv_path = "./output_ixiropoiisi.csv"
    csv_lines = [csv_headers]
    words = list(word2syl.keys())
    for word in words:
        csv_lines += apply_all_rules(word)

    with open(csv_path, "w", encoding="utf-8", newline="") as csvfp:
        csvw = csv.writer(csvfp)
        csvw.writerows(csv_lines)


if __name__ == "__main__":
    main()
    # word = "τιράντες"
    # to_swap = "τ"
    # swap_with = "ν"
    # swap_string_all_combinations(word, to_swap, swap_with)
    # rule = "r_tn"
    # string_sub_rule(word, rule)
    # new_words = swap_string_all_combinations_no_duplicates(
    #             word, to_swap, swap_with
    #         )
    # word = "σφουγγαρίστρα"
    # rule = "r_tn"
    # string_sub_rule(word, rule)
     