import json
import csv
import re
import unicodedata

word2syl = {}
with open("word_to_syllable.json", "r") as f:
    word2syl = json.load(f)


def get_syllables_and_syl_with_accent(word):
    syllables, syl_num, accented_syl = word2syl[word]
    return syllables, accented_syl


rules_abbr2full = {
    "pps": "Πτώση προτονικής συλλαβής",
    "pms": "Πτώση μετατονικής συλλαβής",
    "oa": "Ολικός αναδίπλασιαμός",
    "ma": "Μερικός αναδιπλασιασμός",
    "pts": "Πτώση τελικού συμφώνου",
}

greek_consontants_regex = "β|γ|δ|ζ|θ|κ|λ|μ|ν|ξ|π|ρ|σ|τ|φ|χ|ψ"
greek_vowels_unusual = ["αη", "αϊ", "οη", "οϊ"]
greek_vowels_double = ["αι", "ει", "οι", "ου", "υι", "αυ", "ειυ", "ια"]
greek_vowels_single = ["α", "ε", "η", "ι", "υ", "ο", "ω"]
rules_structural_simplification = ["pps", "pms", "oa", "ma", "pts"]


def apply_all_rules(word):
    # input is a str to have the rules applied to
    # output is a list of 3-member lists, which
    # are ultimately used to populate the csv with headers
    # "Correct","Variation","Rule Name"

    all_mistakes_list = []
    csv_lines = []

    for rule in rules_structural_simplification:
        rule_full = rules_abbr2full[rule]
        # print(rule+" -> " + rule_full)
        mistakes_list = structural_simplification_rule(word, rule)
        all_mistakes_list += mistakes_list
        csv_lines += [[word, x, rule_full] for x in mistakes_list]

    return csv_lines


def remove_acc(input_str):
    nfkd_form = unicodedata.normalize("NFKD", input_str)
    return "".join([c for c in nfkd_form if not unicodedata.combining(c)])


def remove_and_concat(list, x):
    if len(list) < 3:
        return ""
    l2 = list
    try:
        del l2[x]
    except IndexError as e:
        print(e)
        print(list)
        return ""
    return "".join(l2)


def duplicate_and_concat(list, i):
    if i < 0:
        i = len(list) + i
    try:
        elem = list[i]
        l2 = list[:i] + [elem] + list[i:]
    except IndexError:
        return ""
    return "".join(l2)


def duplicate_remove_tilde_and_concat(list, i):
    # l = list
    if i < 0:
        i = len(list) + i
    if i < 1:
        return ""
    try:
        duplicate_elem = list[i]
        duplicate_elem = remove_acc(duplicate_elem)
        if duplicate_elem[-1] == "ς":
            duplicate_elem = duplicate_elem[:-1]
        l2 = list[: i - 1] + [duplicate_elem] + list[i:]
    except IndexError:
        return ""
    return "".join(l2)


def extract_vowels(text):
    return re.sub(greek_consontants_regex, "", text)


def find_first_vowel_segment(text):
    if text in greek_vowels_single:
        return 0, text
    previous_vowel = ""
    for i, char in enumerate(text):
        if previous_vowel != "":
            if char not in greek_vowels_single:
                # found single vowel
                return i - 1, str(previous_vowel)
            if char in greek_vowels_single:
                return i - 1, previous_vowel + char
        elif char in greek_vowels_single:
            previous_vowel = char

    if previous_vowel != "":
        return len(text) - 1, str(previous_vowel)

    return 0, text


def replace_vowel_with(text, vowel):
    ind, first_vowel_of_text = find_first_vowel_segment(text)
    return text.replace(first_vowel_of_text, vowel, 1)


# μηλο->μημο
def partial_duplication_rule(syl_l, acc_ind):
    # e.g. "μήλο" --> "μήμο"
    # "ρί-χνω" --> "ρίρω"
    # this is complicated, let's make it easier
    if acc_ind == -1 or len(syl_l) < 2:
        return ""
    if acc_ind < 0:
        acc_ind = len(syl_l) + acc_ind
    # now the index of the acc is positive
    # step 1, find the vowel (or vowel complex) in the
    # syllable after the accented one
    x, vowel1 = find_first_vowel_segment(syl_l[acc_ind + 1])
    # duplication
    # step 2, duplicate the accented one inside the word,
    # but don't forget to remove the one after the accented one
    new_syl_l = []
    # word up the accented one
    new_syl_l.extend(syl_l[:acc_ind])
    # append the accented syllable
    new_syl_l.append(syl_l[acc_ind])
    # append the accented syllable again, but without the accent
    new_syl_l.append(remove_acc(syl_l[acc_ind]))
    # print(new_syl_l)
    # restore only the vowel part in sylable acc_ind+1
    new_syl_l[acc_ind + 1] = replace_vowel_with(new_syl_l[acc_ind + 1], vowel1)
    # print(new_syl_l[acc_ind+1])
    # add the rest of the word, if it exists
    if len(syl_l) > len(new_syl_l):
        new_syl_l.extend(syl_l[acc_ind + 2 :])
    return "".join(new_syl_l)


def structural_simplification_rule(word, rule):
    syllables, acc_ind = get_syllables_and_syl_with_accent(word)
    syl_l = syllables.split("-")
    # nsyl = len(syl_l)
    res = ""
    # 1. ensure index is not out of range
    if not (~len(syl_l) <= acc_ind <= len(syl_l) - 1):
        print("Wrong accent index (out of bounds)")
        print("not applying structural simplifcation rule: " + rule)
        res = ""
    # moving on to apply the rules
    elif rule == "pps":
        if abs(acc_ind) == len(syl_l) or acc_ind == 0:
            # we enter this block if acc_ind is negative
            # and it points the first syllable, no sense
            # for this rule
            print(
                "can't execute pps rule for word that starts with the stressed syllable"
            )
            print(
                "not applying structural simplifcation rule: " + rule + " for " + word
            )
            res = ""
        else:
            res = remove_and_concat(syl_l, acc_ind - 1)
    elif rule == "pms":
        if acc_ind == -1 or acc_ind == len(syl_l) - 1:
            # no sense in removing the syllable after
            # the accented one if it's the last
            print(
                "can't execute pms rule for word that ends with the stressed syllable"
            )
            print(
                "not applying structural simplifcation rule: " + rule + " for " + word
            )
            res = ""
        else:
            res = remove_and_concat(syl_l, acc_ind + 1)
    elif rule == "oa":
        res = duplicate_remove_tilde_and_concat(syl_l, acc_ind)
    elif rule == "ma":
        res = partial_duplication_rule(syl_l, acc_ind)
    elif rule == "pts":
        res = "".join(syl_l)
        if res[-1] == "ς":
            res = res[:-1]
        else:
            res = ""
    else:
        print("non-applicable rule")
        res = ""

    # should return a list
    if res == "":
        return []
    else:
        return [res]


def main():

    csv_headers = ["Correct", "Variation", "Error Type"]
    csv_path = "./output_structural_simplifications.csv"
    csv_lines = [csv_headers]
    words = list(word2syl.keys())
    for word in words:
        csv_lines += apply_all_rules(word)
        # print(csv_lines)

    with open(csv_path, "w", encoding="utf-8", newline="") as csvfp:
        csvw = csv.writer(csvfp)
        csvw.writerows(csv_lines)


if __name__ == "__main__":
    main()
