import json
import csv
from utils import swap_string_all_combinations, swap_string_all_combinations_no_duplicates

with open("word_to_syllable.json", "r") as f:
    word2syl = json.load(f)

rules_abbr2full = {
    "our_r_gi": "Ουρανικοποίηση ρ - γι",
    "our_l_gi": "Ουρανικοποίηση λ - γι",
    "our_f_xi": "Ουρανικοποίηση φ - xι",
    "our_b_gi": "Ουρανικοποίηση β - γι",
    "our_s_xi": "Ουρανικοποίηση σ - χι",
    "our_z_gi": "Ουρανικοποίηση ζ - γι",
    "our_th_xi": "Ουρανικοποίηση θ - χι",
    "our_d_gi": "Ουρανικοποίηση δ - γι",
    "our_x_xi": "Ουρανικοποίηση χ - χι",
    "our_g_gi": "Ουρανικοποίηση γ - γι",
    "our_ts_xi": "Ουρανικοποίηση τσ - χι",
    "our_ntz_gi": "Ουρανικοποίηση ντζ - γι",
    "our_tz_gi": "Ουρανικοποίηση τζ - γι",
    "our_r_l": "Ουρανικοποίηση ρ - λ",
}

rules_subst = {
    "our_r_gi": {"ρ": "γι"},
    "our_l_gi": {"λ": "γι"},
    "our_f_xi": {"φ": "xι"},
    "our_b_gi": {"β": "γι"},
    "our_s_xi": {"σ": "χι"},
    "our_z_gi": {"ζ": "γι"},
    "our_th_xi": {"θ": "χι"},
    "our_d_gi": {"δ": "γι"},
    "our_x_xi": {"χ": "χι"},
    "our_g_gi": {"γ": "γι"},
    "our_ts_xi": {"τσ": "χι"},
    "our_ntz_gi": {"ντζ": "γι"},
    "our_tz_gi": {"τζ": "γι"},
    "our_r_l": {"ρ": "λ"},
}

greek_consontants_regex = "β|γ|δ|ζ|θ|κ|λ|μ|ν|ξ|π|ρ|σ|τ|φ|χ|ψ"
greek_vowels_unusual = ["αη", "αϊ", "οη", "οϊ"]
greek_vowels_double = ["αι", "ει", "οι", "ου", "υι", "αυ", "ειυ", "ια"]
greek_vowels_single = ["α", "ε", "η", "ι", "υ", "ο", "ω"]


greek_i = ["η", "ι", "υ", "ει", "οι", "υι", "ειυ"]
greek_e = ["ε", "αι"]


def string_sub_rule(word, rule):
    if rule not in rules_subst:
        print("No such rule!")
    else:
        mistakes_list = []
        for substitution in rules_subst[rule]:
            to_swap = substitution
            print(to_swap)
            swap_with = rules_subst[rule][substitution]
            print(swap_with)
            new_words = swap_string_all_combinations_no_duplicates(word, to_swap, swap_with)
            mistakes_list = mistakes_list + new_words
    print(mistakes_list)
    return mistakes_list


def apply_all_rules(word):
    csv_lines = []
    for rule in rules_abbr2full.keys():
        rule_full = rules_abbr2full[rule]
        try:
            mistakes_list = string_sub_rule(word, rule)
        except NotImplementedError:
            print("WARNING!: Rule " + rule_full + " not implemented yet!")
            continue

        csv_lines += [[word, x, rule_full] for x in mistakes_list]
    return csv_lines


def main():

    csv_headers = ["Correct", "Variation", "Error Type"]
    csv_path = "./output_ouranikopoiisi.csv"
    csv_lines = [csv_headers]
    words = list(word2syl.keys())
    for word in words:
        csv_lines += apply_all_rules(word)

    with open(csv_path, "w", encoding="utf-8", newline="") as csvfp:
        csvw = csv.writer(csvfp)
        csvw.writerows(csv_lines)


if __name__ == "__main__":
    main()
    # word = "τιράντες"
    # # to_swap = "τ"
    # # swap_with = "τσ"
    # # swap_string_all_combinations(word, to_swap, swap_with)
    # rule = "r_tn"
    # string_sub_rule(word, rule)
