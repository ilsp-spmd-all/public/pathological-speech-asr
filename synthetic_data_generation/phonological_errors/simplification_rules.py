import json
import csv
import unicodedata

rules_abbr2full = {
    "aplop_eks_y": "Απλοποίηση συμπλεγμάτων: Εξακολουθητικό και υγρό",
    "aplop_eks_stigm": "Απλοποίηση συμπλεγμάτων: Εξακολουθητικό και στιγμιαίο",
    "aplop_eks_enr": "Απλοποίηση συμπλεγμάτων: Εξακολουθητικό και έρρινο",
    "aplop_eks_eks": "Απλοποίηση συμπλεγμάτων: Εξακολουθητικό και εξακολουθητικό",
    "aplop_s_stigm": "Απλοποίηση συμπλεγμάτων: s και στιγμιαίο",
    "aplop_eks_stigm_y": "Απλοποίηση συμπλεγμάτων: \
Εξακολουθητικό και στιγμιαίο και υγρό",
    "aplop_eks_stigm_eks": "Απλοποίηση συμπλεγμάτων: \
Εξακολουθητικό και στιγμιαίο και εξακολουθητικό",
    "aplop_stigm_y": "Απλοποίηση συμπλεγμάτων: Στιγμιαίο και υγρό",
    "aplop_stigm_eks": "Απλοποίηση συμπλεγμάτων: Στιγμιαίο και εξακολουθητικό",
    "aplop_stigm_enr": "Απλοποίηση συμπλεγμάτων: Στιγμιαίο και έρρινο",
    "aplop_enr_enr": "Απλοποίηση συμπλεγμάτων: Έρρινο και έρρινο",
}

affected_compounds_per_rule = {
    "aplop_eks_y": ["γλ", "φλ", "βλ", "θρ", "βρ", "φρ", "γρ", "δρ"],
    "aplop_eks_stigm": ["χτ", " φτ"],
    "aplop_eks_enr": ["χν", " ζμ"],
    "aplop_eks_eks": ["δγ", " ζγ", "σφ", "σχ", "βγ"],
    "aplop_s_stigm": ["σκ", "σπ", "στ"],
    "aplop_stigm_y": ["πλ", "κλ", "τρ", "κρ", "ντρ"],
    "aplop_eks_stigm_y": ["στρ", "χτρ"],
    "aplop_eks_stigm_eks": ["φτχ"],
    "aplop_stigm_eks": ["μπγ", "πχ", "τχ"],
    "aplop_stigm_enr": ["κν", " πν"],
    "aplop_enr_enr": ["μν"],
}

rules_consonant_simplification = [
    "aplop_eks_y",
    "aplop_eks_stigm",
    "aplop_eks_enr",
    "aplop_eks_eks",
    "aplop_s_stigm",
    "aplop_eks_stigm_y",
    "aplop_eks_stigm_eks",
    "aplop_stigm_y",
    "aplop_stigm_eks",
    "aplop_stigm_enr",
    "aplop_enr_enr",
    "aplop_stigm_eks",
]


def remove_acc(input_str):
    nfkd_form = unicodedata.normalize("NFKD", input_str)
    return "".join([c for c in nfkd_form if not unicodedata.combining(c)])


word2syl = {}
with open("word_to_syllable.json", "r") as f:
    word2syl = json.load(f)


def get_syllables(word):

    syllables, syl_num, accented_syl = word2syl[word]
    return syllables, accented_syl


def consonant_complex_simplification_rule(w, r):
    if r in [
        "aplop_stigm_y",
        "aplop_eks_stigm_y",
        "aplop_eks_stigm_eks",
        "aplop_stigm_eks",
    ]:
        # Immanuel... you have to complete this
        raise NotImplementedError
    results = []
    compounds_list = affected_compounds_per_rule[r]
    for c in compounds_list:
        if c in w:
            c_reduced = c[:-1]
            variation = w.replace(c, c_reduced, 1)
            results.append(variation)
            c_reduced_inverse = c[1:]
            variation_inverse = w.replace(c, c_reduced_inverse, 1)
            results.append(variation_inverse)

    return results


def apply_all_rules(word):
    csv_lines = []
    for rule in rules_consonant_simplification:
        rule_full = rules_abbr2full[rule]
        try:
            mistakes_list = consonant_complex_simplification_rule(word, rule)
        except NotImplementedError:
            print("WARNING!: Rule " + rule_full + " not implemented yet!")
            continue

        csv_lines += [[word, x, rule_full] for x in mistakes_list]
    return csv_lines


def main():

    csv_headers = ["Correct", "Variation", "Error Type"]
    csv_path = "./output_simplification_complex_consonants.csv"
    csv_lines = [csv_headers]
    words = list(word2syl.keys())
    for word in words:
        csv_lines += apply_all_rules(word)

    with open(csv_path, "w", encoding="utf-8", newline="") as csvfp:
        csvw = csv.writer(csvfp)
        csvw.writerows(csv_lines)


if __name__ == "__main__":
    main()
