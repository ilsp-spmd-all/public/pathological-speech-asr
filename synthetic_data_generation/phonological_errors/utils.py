from itertools import chain, combinations
import unicodedata as ud

def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s) + 1))

def remove_diacritics(word):
    d = {ord('\N{COMBINING ACUTE ACCENT}'):None}
    word=ud.normalize('NFD',word).translate(d)
    return word
    


def find_double_letters(word):
    doubles=[]
    temp=word[0]
    print(temp)
    for ind,char in enumerate(word[1:]):
        print(char,ind)
        if char==temp[-1]:
            doubles.append((char,ind))
            continue
        temp+=char
    print(temp,doubles)
    return temp,doubles

def restore_double_letters(word,letter_ind_list):
    # letter_ind_list is a list of "letter","index" tuples
    # function injects the letters to the corresponding index in the word string
    for letter,ind in letter_ind_list:
        word=word[:ind]+letter+word[ind:]
        print(word)
        

def replace_occ_from_index_list(word, to_swap, swap_with, inds):
    new_word=word
    inds=list(inds)
    inds.sort(reverse = True)
    new_word = list(word)
    for ind in inds:
        new_word=word[:ind]+swap_with+word[ind+len(to_swap):]
    return new_word


def replace_occ_from_index_list_no_duplicates(word, to_swap, swap_with, inds):
    new_word=word
    inds=list(inds)
    inds.sort(reverse = True)
    print(inds)
    for ind in inds:
        new_word=new_word[:ind]+swap_with+new_word[ind+len(to_swap):]
        # next four lines check whether after swapping there will be a duplicate letter
        # if yes, they delete one of the two identical letters
        if ind>0:
            if remove_diacritics( list(word)[ind - 1]) == remove_diacritics( swap_with[0]):
                new_word=new_word[:ind]+new_word[ind+1:]
        
        if len(list(word))>=(ind+len(to_swap)+1):
            if remove_diacritics(list(word)[ind + len(to_swap)]) == remove_diacritics(swap_with[-1]):
                new_word=new_word[:ind+len(to_swap)]+new_word[ind+len(to_swap)+1:]
    return new_word


def find_double_letters(word):
    doubles=[]
    temp=word[0]
    print(temp)
    for ind,char in enumerate(word[1:]):
        print(char,ind)
        if char==temp[-1]:
            doubles.append((char,ind))
            continue
        temp+=char
    print(temp,doubles)
    return temp,doubles

def restore_double_letters(word,letter_ind_list):
    # letter_ind_list is a list of "letter","index" tuples
    # function injects the letters to the corresponding index in the word string
    for letter,ind in letter_ind_list:
        word=word[:ind]+letter+word[ind:]
        print(word)
        

def replace_occ_from_index_list(word, to_swap, swap_with, inds):
    new_word = list(word)
    inds=list(inds)
    inds.sort(reverse = True)
    new_word = list(word)
    for ind in inds:
        # the line below deletes the letters of the "to_swap" substring
        new_word[ind + 1 : (ind + len(to_swap))] = ""
        # the line below adds the "swap_with" substring in place of the deleted letters
        # note that the whole substring is put into one list position
        new_word[ind] = swap_with
    new_word = "".join(new_word)
    return new_word


def replace_occ_from_index_list_no_duplicates(word, to_swap, swap_with, inds):
    new_word = list(word)
    inds=list(inds)
    inds.sort(reverse = True)
    print(inds)
    print(new_word)
    for ind in inds:
        print(ind)
        # the line below deletes the letters of the "to_swap" substring
        new_word[ind + 1 : (ind + len(to_swap))] = ""
        print("dfads")
        # the line below adds the "swap_with" substring in place of the deleted letters
        # note that the whole substring is put into one list position
        new_word[ind] = swap_with
        # next four lines check whether after swapping there will be a duplicate letter
        # if yes, they delete one of the two identical letters
        if list(word)[ind - 1] == swap_with[0]:
            new_word[ind - 1] = ""
        if list(word)[ind + len(to_swap)] == swap_with[-1]:
            new_word[ind + len(to_swap)] = ""
    new_word = "".join(new_word)
    return new_word


def swap_string_first(word, to_swap, swap_with):

    # input is "word", a string, output is "new_word"
    # swaps the first "to_swap" string found in "word" with the "swap_with string"

    letter_inds = [i for i in range(len(word)) if word.startswith(to_swap, i)]
    if len(letter_inds) > 0:
        ind = [
            letter_inds[0]
        ]  # put in single element list to input into replace_occ_from_index_list
        new_word = replace_occ_from_index_list(word, to_swap, swap_with, ind)
        return new_word
    else:
        print(f'no "{to_swap}"s in word {word}')
        return


def swap_string_all(word, to_swap, swap_with):

    # input is "word", a string, output is "new_word"
    # swaps "to_swap" string if found in "word" with the "swap_with string"
    # for multiple strings, it swaps all of them and returns the word
    letter_inds = [i for i in range(len(word)) if word.startswith(to_swap, i)]
    if len(letter_inds) > 0:
        new_word = replace_occ_from_index_list(word, to_swap, swap_with, letter_inds)
        return new_word
    else:
        print(f'no "{to_swap}"s in word {word}')
        return


def swap_string_all_combinations(word, to_swap, swap_with):

    # input is "word", a string, output is a list of new_words
    # swaps "to_swap" string if found in "word" with the "swap_with string"
    # for multiple strings, it swaps all of them
    # to change behaviour: we want if there are multiple "to_swap" occurences to return
    # all combinations of swapped versions
    letter_inds = [i for i in range(len(word)) if word.startswith(to_swap, i)]

    if len(letter_inds) > 0:
        combination_inds = list(powerset(letter_inds))[1:]
        # print(combination_inds)
        new_words = []
        for inds in combination_inds:
            new_word = replace_occ_from_index_list(word, to_swap, swap_with, inds)
            new_words.append(new_word)
        return new_words
    else:
        print(f'no "{to_swap}"s in word {word}')
        return []


def swap_string_all_combinations_no_duplicates(word, to_swap, swap_with):

    # input is "word", a string, output is a list of new_words
    # swaps "to_swap" string if found in "word" with the "swap_with string"
    # for multiple strings, it swaps all of them
    # to change behaviour: we want if there are multiple "to_swap" occurences to return
    # all combinations of swapped versions
    
    #first we remove double letters (not implemented yet)
    # word,letters=find_double_letters(word)
    letter_inds = [i for i in range(len(word)) if word.startswith(to_swap, i)]
    print(word)
    if len(letter_inds) > 0:
        combination_inds = list(powerset(letter_inds))[1:]
        print(combination_inds)
        new_words = []
        for inds in combination_inds:
            new_word = replace_occ_from_index_list_no_duplicates(
                word, to_swap, swap_with, inds
            )
            new_words.append(new_word)
            print(new_word)
        print(new_words)
        return new_words
    else:
        print(f'no "{to_swap}"s in word {word}')
        return []


if __name__ == "__main__":
    word="ντομάτα"
    to_swap="τ"
    swap_with="ν"
    # inds=[0]
    # print(swap_string_all_combinations_no_duplicates(word,"τσ","σ"))
    # word,letters=find_double_letters("αμμοννι")
    # restore_double_letters(word,letters)
    # pass
    # new_words=replace_occ_from_index_list_no_duplicates(word, to_swap, swap_with, inds)
    new_words=swap_string_all_combinations_no_duplicates(word, to_swap, swap_with)
    print(new_words)
