# for these rules, we are replacing with vowels sometimes
# so be careful of accents: when replacing accented voewls use accented

import json
import csv
from utils import (
    swap_string_all_combinations,
    swap_string_all_combinations_no_duplicates,
)

with open("word_to_syllable.json", "r") as f:
    word2syl = json.load(f)

rules_abbr2full = {
    "st_f_p": "Στιγμικοποίηση φ - π",
    "st_b_mp": "Στιγμικοποίηση β - μπ",
    "st_u_p": "Στιγμικοποίηση θ - π",
    "st_u_t": "Στιγμικοποίηση θ - τ",
    "st_d_mp": "Στιγμικοποίηση δ - μπ",
    "st_d_nt": "Στιγμικοποίηση δ - ντ",
    "st_s_t": "Στιγμικοποίηση σ - τ",
    "st_z_nt": "Στιγμικοποίηση ζ - ντ",
    # "st_ts_t": "Στιγμικοποίηση τσ - τ",
    "st_ntz_nt": "Στιγμικοποίηση ντζ - ντ",
    "st_tz_nt": "Στιγμικοποίηση τζ - ντ",
    "st_x_k": "Στιγμικοποίηση χ - κ",
    "st_g_gk": "Στιγμικοποίηση γ - γκ",
    "st_l_nt": "Στιγμικοποίηση λ - ντ",
    "st_r_nt": "Στιγμικοποίηση ρ - ντ",
    # "tr_r_nt": "Τριβικοποίηση κι - χι", #
    "tr_r_nt": "Τριβικοποίηση κ - χ",
    "tr_r_nt": "Τριβικοποίηση γκα - γα",
    "tr_r_nt": "Τριβικοποίηση γκι - γι",
    
}

rules_subst = {
    "st_f_p": {"φ":"π"},
    "st_b_mp": {"β":"μπ"},
    "st_u_p": {"θ":"π"},
    "st_u_t": {"θ":"τ"},
    "st_d_mp": {"δ":"μπ"},
    "st_d_nt": {"δ":"ντ"},
    "st_s_t": {"σ":"τ"},
    "st_z_nt": {"ζ":"ντ"},
    # "st_ts_t": {"τσ":"τ"},
    "st_ntz_nt": {"ντζ":"ντ"},
    "st_tz_nt": {"τζ":"ντ"},
    "st_x_k": {"χ":"κ"},
    "st_g_gk": {"γ":"γκ"},
    "st_l_nt": {"λ":"ντ"},
    "st_r_nt": {"ρ":"ντ"},
    # "tr_r_nt": "Τριβικοποίηση κι":"χι",
    "tr_r_nt": {"κ":"χ"},
    "tr_r_nt": {"γκα":"γα","γγα":"γα" },
    "tr_r_nt": {"γκι":"γι", "γγι":"γι"},
}

greek_consontants_regex = "β|γ|δ|ζ|θ|κ|λ|μ|ν|ξ|π|ρ|σ|τ|φ|χ|ψ"
greek_vowels_unusual = ["αη", "αϊ", "οη", "οϊ"]
greek_vowels_double = ["αι", "ει", "οι", "ου", "υι", "αυ", "ειυ", "ια"]
greek_vowels_single = ["α", "ε", "η", "ι", "υ", "ο", "ω"]


def string_sub_rule(word, rule):
    if rule not in rules_subst:
        print("No such rule!")
    else:
        mistakes_list = []
        for substitution in rules_subst[rule]:
            to_swap = substitution
            print(to_swap)
            swap_with = rules_subst[rule][substitution]
            print(swap_with)
            new_words = swap_string_all_combinations_no_duplicates(
                word, to_swap, swap_with
            )
            mistakes_list = mistakes_list + new_words
    print(mistakes_list)
    return mistakes_list


def apply_all_rules(word):
    csv_lines = []
    for rule in rules_abbr2full.keys():
        rule_full = rules_abbr2full[rule]
        try:
            mistakes_list = string_sub_rule(word, rule)
        except NotImplementedError:
            print("WARNING!: Rule " + rule_full + " not implemented yet!")
            continue

        csv_lines += [[word, x, rule_full] for x in mistakes_list]
    return csv_lines


def main():

    csv_headers = ["Correct", "Variation", "Error Type"]
    csv_path = "./output_stigmika_trivomena.csv"
    csv_lines = [csv_headers]
    words = list(word2syl.keys())
    for word in words:
        csv_lines += apply_all_rules(word)

    with open(csv_path, "w", encoding="utf-8", newline="") as csvfp:
        csvw = csv.writer(csvfp)
        csvw.writerows(csv_lines)


if __name__ == "__main__":
    main()
    # word = "τιράντες"
    # to_swap = "τ"
    # swap_with = "ν"
    # swap_string_all_combinations(word, to_swap, swap_with)
    # rule = "r_tn"
    # string_sub_rule(word, rule)
    # new_words = swap_string_all_combinations_no_duplicates(
    #             word, to_swap, swap_with
    #         )
    # word = "σφουγγαρίστρα"
    # rule = "r_tn"
    # string_sub_rule(word, rule)
     