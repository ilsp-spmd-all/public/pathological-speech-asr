# Generate phonological errors

This process applies a number of rules that generate phonological errors, e.g. `"σχ"->"χ"`. The rules are defined inside `rules.py`, which is also the `main` of this small application. The results are stored in a csv of the form:

` initial word | variation/mistake | production rule`.

An example of this csv stored [here](https://docs.google.com/spreadsheets/d/1c5_iIGS8F0SZpG5FCSrOS76l58GSIjOI3UfFNnWx60A/edit?usp=sharing). Rules described [here](https://imisathena.sharepoint.com/:x:/s/Plan-V/EbaiJpApSIBPtt_UB2aS3UMBDHcCF4rF1OmWMjhJL8VLTA?e=DNXMLS)


## Usage

### Step 1 - Prepare input targets
This process starts with a list of target words (stored as `target_list.txt`). We then proceed to 
create the syllables file (`target_list_syllables.txt`) using a tool that splits the words into syllables, separated with hypens (`-`). [Here is an example of such a tool](https://melobytes.gr/el/app/syllavismos). 

The way the syllabism is done is very important: it should contain no mistakes. Also, we have better results if we split phonologically: e.g. "αγγούρι" is normally split as"αγ-γού-ρι", but phonetically it should be "α-γγού-ρι".

TODO: add a script that does the phonetical splitting.

Finally, we use the the syllables file to make the word2syl map, which has this form:

```file="word_to_syllable.json
{
    "γάτα": [
        "γά-τα",
        2,
        -2
    ],
    "αγγούρι": [
        "αγ-γού-ρι",
        3,
        -2
    ],
    "αναπτήρας": [
        "α-να-πτή-ρας",
        4,
        -2
    ],
    "αδιάβροχο": [
        "α-διά-βρο-χο",
        4,
        -3
    ],
    "λύκος": [
        "λύ-κος",
        2,
        -2
    ],
    "αγκινάρες": [
        "α-γκι-νά-ρες",
        4,
        -2
    ]
}

```
### Step 2 - Generate the csv with the errors

First store the `words_to_syllables.json` file in the same folder as the rules files (where this readme is also stored). Then run the rules script:

```
python3 apply_rules.py simplification_rules structural_simplification_rules
```

The result is stored in the `output.csv` file.

## Rules Available

Structural Simplification Rules: structural_simplification_rules.py
Simplification Rules: simplification_rules.py
Posterior <-> Anterior: prosthia_opisthia.py
Nasalification: nasal.py


## Adding rules

To add a new set of rules (or even a single rule), create a python file that includes the definition of a function named `apply_all_rules(word: str) -> List[List[str]]` as follows:
```
def apply_all_rules(word):
    ## input is a single word
    all_mistakes_list=[]
    csv_lines=[]
    mistakes_list=run_rules(word)

    ## output format is a list of 3member lists, which
    ## are used to populate the csv with headers
    ## "Correct","Variation","Rule Name"
    csv_lines+=[[word,x,"my_rulename"] for x in mistakes_list]

    return csv_lines
```

Take as examples the files `structural_simplification_rules.py` and `simplification_rules.py`. You can use the `words_to_syllables.json`
