import sys
import csv
import json
import importlib
word2syl={}
with open("word_to_syllable.json",'r') as f:
    word2syl=json.load(f)

rule_modules=[]
for arg in sys.argv[1:]:
    try:
        rule_modules.append(importlib.import_module(arg))
    except ModuleNotFoundError:
        print("No module named " + arg)
        continue
    print("Adding rule from module " + arg)

csv_headers=["Correct","Variation","Error Type"]
csv_lines=[csv_headers]
csv_path="./output.csv"

words=list(word2syl.keys())
for word in words: 
    for rulemod in rule_modules:
        csv_lines+=rulemod.apply_all_rules(word)


with open(csv_path, 'w', encoding='utf-8', newline='') as csvfp:
   csvw=csv.writer(csvfp)
   csvw.writerows(csv_lines)
