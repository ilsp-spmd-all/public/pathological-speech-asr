# S4A Voice Assistant

A set of microservices that turn a chatbot into a voice bot. Also some clients.

This branch is dedicated to safety4all. We will cherry-pick bug fixes and patches back into main but it can diverge.

# Main components

- Master
- ASR (google)
- TTS (google)
- S4A dialogue system (rasa)

## Architecture

Based on dockerized microservices, easy to setup with Kubernetes.

![Architecture diagram](arch_diagram_VA.png)

## Prerequisites

- ~10GB disk space
- 4-8 GB RAM
- python3.7.3, docker and docker-compose installed

## Setup

1. First, clone the repo with

```
git clone https://gitlab.com/ilsp-spmd-all/dialogue/generic-voice-assistant
```

2. Then copy and paste the chatbot folder on the same level as the microservices (in our case, the root folder of the repo).

```
cp -r my_rasa_chatbot /path/to/this/repo
```

3. Thirdly get the google creds file and store it inside the asr and tts dirs

```
cp google.json /path/to/this/repo/asr_server
cp google.json /path/to/this/repo/tts_server
```

4. Store the https keys in the master_server folder:

```
mkdir master_server/keys
cp <your_server_certificate_file> master_server/keys/server.cert
cp <your_server_privkey_file> master_server/keys/server.key
```

5. Create docker-compose override to set the port to await websocket connections on.

```
cp docker-compose.override-generic.yml docker-compose.override.yml
vi docker-compose.override.yml
```

6. Finally, to start the service, use the following command:

```
docker-compose build && docker-compose up
```

If you want to interact with your VA, you need to use one of the available clients. For example, run `clients/web_client.html` -
if it doesn't connect, you might need to set the MASTER_HOST variable to the host where the
service is running.

ATTENTION! If you want to run client on the same host, don't use localhost, use the domain name stored in the certificate!
