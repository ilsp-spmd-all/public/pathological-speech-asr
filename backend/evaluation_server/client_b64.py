import asyncio
import json
import base64
import sys

import websockets

import constants
from gspeech import chunked_audio_file

SLEEP_BETWEEN_CHUNKS=False
TIME_BETWEEN_CHUNKS=0.8

class AudioStream:
    """Opens a recording stream as a generator yielding the audio chunks."""

    def __init__(
        self,
        fname: str,
        sr: int = 44100,
        chunk_size_ms: int = 100,
        endpoint: str = "audio-evaluation-ws",
        host: str = "dialogue.ilsp.gr",
        port: int = 8894,
    ):
        self._url = f"wss://{host}:{port}/{endpoint}"
        self.closed = True
        self._fname = fname
        self._sr = sr
        self._chunk_size = chunk_size_ms

    def __enter__(self):
        self.closed = False

        self._chunks = chunked_audio_file(
            self._fname, chunk_size_ms=self._chunk_size, sr=self._sr
        )

        return self

    def __exit__(self, type, value, traceback):
        self.closed = True
        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.

    async def produce(self):
        # chunks = (
        #     [constants.RESTART_COMMUNICATION]
        #     + list(self._chunks)
        #     + [constants.END_OF_STREAM]
        # )

        chunks = (
            [constants.RESTART_COMMUNICATION]
            + list(self._chunks)
            # + [constants.END_OF_STREAM]
        )

        async with websockets.connect(self._url) as asr_client:

            await asr_client.send(chunks[0])

            for chunk in chunks[1:]:
                string_to_send=base64.b64encode(chunk).decode('ascii')
                inverse_trans =base64.b64decode(string_to_send)
                if inverse_trans == chunk:
                    #print('success')
                    pass

                else:
                    print('wtf')
                await asr_client.send(string_to_send)
                if SLEEP_BETWEEN_CHUNKS:
                    await asyncio.sleep(TIME_BETWEEN_CHUNKS)
                #print(string_to_send[:100])

            responses = []

            while True:
                resp = await asr_client.recv()
                responses.append(resp)

                if resp == constants.END_OF_TRANSCRIPTION:
                    print(responses[-2])

                    break
                sys.stdout.write(resp + "\r")
                sys.stdout.flush()


def main():
    loop = asyncio.get_event_loop()
    with AudioStream(sys.argv[1]) as stream:
        task1 = [stream.produce()]

    loop.run_until_complete(asyncio.wait(task1))
    loop.close()


if __name__ == "__main__":
    main()
