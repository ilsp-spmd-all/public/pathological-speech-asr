# Reimplementation of ASR websocket server


To run:

```
export GOOGLE_APPLICATION_CREDENTIALS=/path/to/google.json
pip install -r requirements.txt
uvicorn --port 8001 evaluation_server:app
python client.py path/to/test.wav  # 16000 Hz, single channel
```
