
# test_from_file.py

from __future__ import division
import re
import base64
import json
import argparse
import sys
import websocket 
import constants
# from websocket import create_connection
import time
from multiprocessing import Process,Queue
from pydub import AudioSegment
from pydub.utils import make_chunks
import ssl
PORT=8001
# ENDPOINT="wss://borges.ilsp.gr:"+ str(PORT) + "/audio-evaluation-sentences-ws"
ENDPOINT="ws://127.0.0.1:"+ str(PORT) + "/audio-evaluation-ws"
MODE='b64'
# MODE='bytes'


''' This script takes a single channel wav file as an argument, and
    streams it to google for transcribing 
    It can be used with flag --no_interim.
    
    Here we instantiate an ASR client. Then main() calls method run().
    In essence, this method activates the client. It spawns two processes.
    One (run_record_and_send() ) is for sending the audio chunk by chunk between 100ms intervals.
    The other (run_receive_and_print) is for handling the incoming responses from the asr server.

    Arguments:
     
     1 argument:  the filename of the wav file. It must be a 16 bit Signed Int PCM file, 44100Hz.

    Flags:
     
     --no-interim: do not send interim results.
    '''

def id(x):
    # Good ol' identity function. 
    # For Debugging reasons.

    return x

def audio_chunks_from_file_generator(filename):
    #this simulates the streaming microphone using an input file

    myaudio = AudioSegment.from_file(filename , "wav") 
    chunk_length_ms = 100 # pydub calculates in millisec
    chunks = make_chunks(myaudio, chunk_length_ms) #Make chunks of one sec
    print("no_chunks:",len(chunks))
    for i, chunk in enumerate(chunks):
            print("chunk: ",i)
            print("chunk: ",chunk)
            #print("chunk*")
            time.sleep(0.1)
            yield chunk.raw_data
    
class ASRClient(object):

    def __init__(self):
        self.isClosed = False #TODO implement closing mechanism
        self.ws= websocket.create_connection(ENDPOINT)
        # self.ws= create_connection(ENDPOINT,sslopt={"cert_reqs": ssl.CERT_NONE})

    def incoming_stream_generator(self):
        #print("awaiting incoming messages")
        while (self.isClosed == False):
            result = self.ws.recv() #awaiting, hopefully it is not blocking
            #print("DEBUG:received message")
            yield result

    def run(self,filename,no_interim,crash,crash_after):
        #main function
        p1= Process(target=self.run_record_and_send,args=(filename,))
        p2= Process(target=self.run_receive_and_print,args=(no_interim,))
        p1.start()
        p2.start()
        if crash:
            time.sleep(crash_after)
            p2.terminate()
        p2.join()
        p1.terminate()

    def run_record_and_send(self,filename):
        # send the audio chunk by chunk between 100ms intervals.
        #self.ws.send("_USER_Κεμάλ:123456")
        if MODE=='bytes':
            self.ws.send("RESTART_COMMUNICATION")
            audio_generator = audio_chunks_from_file_generator(filename)
            for chunk in audio_generator:
                self.ws.send_binary(chunk)
        else:
            # self.ws.send('{"is_start":true,"target":"Γατά!","is_chunk":false,"base64_chunk":"","is_end_of_stream":false}')
            start_dict={}
            start_dict["is_start"]=True
            start_dict["target"]=""
            start_dict["target_sentence"]="Η γάτα ανέβηκε στο παράθυρο."
            start_dict["is_chunk"]=False
            start_dict["base64_chunk"]=""
            start_dict["is_end_of_stream"]=False
            start_dict["filename"]="rec_2"
            start_dict["pageNum"]=2
            start_dict["plan_temp_id"]=18
            start_dict["item_id"]=3
            start_dict["user_id"]="p-uuid"
            self.ws.send(json.dumps(start_dict))

            # self.ws.send('{"is_start":true,"target":"","target_sentence":"Η γάτα ανέβηκε στο παράθυρο.","is_chunk":false,"base64_chunk":"","is_end_of_stream":false,"filename":"rec_2","pageNum":2,"plan_temp_id":18,"item_id":4,"user_id":"p-uuid"}')
            # self.ws.send('{"is_start":true,"target":"Η γάτα ανέβηκε στο παράθυρο.","is_chunk":false,"base64_chunk":"","is_end_of_stream":false,"filename":"rec_2","pageNum":2,"plan_temp_id":18,"item_id":4,"user_id":"p-uuid"}')
            audio_generator = audio_chunks_from_file_generator(filename)
            for chunk in audio_generator:
                string_to_send=base64.b64encode(chunk).decode('ascii')
                new_dict={}
                new_dict["is_start"]=False
                new_dict["target"]=""
                new_dict["target_sentence"]=""
                new_dict["base64_chunk"]=string_to_send
                new_dict["is_chunk"]=True
                new_dict["is_end_of_stream"]=False
                new_dict["filename"]=""
                new_dict["pageNum"]=0
                new_dict["plan_temp_id"]=0
                new_dict["item_id"]=0
                new_dict["user_id"]=""
                self.ws.send(json.dumps(new_dict))
            new_dict={}
            new_dict["is_start"]=False
            new_dict["target"]=""
            new_dict["target_sentence"]=""
            new_dict["base64_chunk"]=""
            new_dict["is_chunk"]=False
            new_dict["is_end_of_stream"]=True
            new_dict["filename"]=""
            new_dict["pageNum"]=0
            new_dict["plan_temp_id"]=0
            new_dict["item_id"]=0
            new_dict["user_id"]=""
            self.ws.send(json.dumps(new_dict))
#             {
#   "is_start": false,
#   "is_chunk": false,
#   "target": "",
#   "target_sentence": "",
#   "base64_chunk": "",
#   "is_end_of_stream": true
# }
# "filename": "rec_2",
# "pageNum": 2,
# "plan_temp_id": 18,
# "item_id": 4,
# "user_id": "p-uuid"
            

    def run_receive_and_print(self,no_interim):
        """Handles the incoming responses from the asr server.

        It takes care of printing the transcript (both interim and final results)
        in a nicely formatted way, making sure that the every interim overwrites 
        the previous one. It also checks if the incoming message is 
        "SINGLE_UTTERANCE_END", in which case we stop handling when we see
        the next final result, using the breakit flag.

        """
        incoming_stream = self.incoming_stream_generator()
        responses = (x for x in incoming_stream)
        prev_response_length=0
        breakit=False
        # The following loop processes the incoming response stream (as a result of a generator) 
        for response in responses:
            # the response could either be interim (if we have interim enabled)
            # or final. 'Final' means that google has made its final decision
            # upon the transcript, and that it will continue to proceed the next 
            # utterance. However, up until now, we are working with single_utterance_mode,
            # which means that we only the first utterance will be transcribed. 

            if response=="SINGLE_UTTERANCE_END":
                # This message is sent by the server when it has detected
                # the end of the first utterance. Unfortunately, it is also
                # the last that is transcribed, as Google actually 
                # doesn't provide this signal if we ask it to 
                # keep transcribing after the first utterance. 
                # Now we will set a flag to True, meaning that 
                # as soon as we get the next (also the first) final 
                # result we will
                # stop awaiting for responses.

                breakit=True
                continue

            # else, it is either an Interim or a Final result. 
            overwrite_chars=' ' * (prev_response_length-len(response)) #to overwrite the previous one

            #Interim results are prefixed with _INTERIM by the server.
            substring=response[:8]
            if substring=="_INTERIM" and no_interim==False:
                    #we print the result and we return to the beginning of the line.
                    # we do this because we want the following result to overwrite the
                    # previous output. (This is how we achieve this smooth printing of
                    # the interims and, in the end, the final result"
                    sys.stdout.write(response+overwrite_chars + '\r')
                    sys.stdout.flush()
                    prev_response_length=len(response)
            elif substring[:4]=="_MP3":
                    print(response)
                    print("Exiting...")

            elif substring!="_INTERIM":
                print(response+overwrite_chars)

                if re.search(r'\b(τερματισμός|stop)\b', response, re.I):
                    #print('DEBUG: Exiting..')
                    break
 
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", type=str, help="source file")
    parser.add_argument("--crash", nargs=1,help="Crash after some time")
    #parser.add_argument("--crash",help="Crash after some time",action='store_true')
    parser.add_argument("--no-interim",help="Disable display of interim results",action='store_true')
    args=parser.parse_args()
    print(args.crash)
    newClient = ASRClient()
    time.sleep(1)
    if args.crash:
        crash=True
        crash_after=int(args.crash[0])
    else: 
        crash=False
        crash_after=-1
    newClient.run(args.filename,args.no_interim,crash,crash_after)
    print("waiting for con to close on the server side")
    time.sleep(4)
    

if __name__ == '__main__':
    main()

