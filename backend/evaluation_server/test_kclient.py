import sys
import time
import kaldigrpc_client as k
from pydub import AudioSegment
from pydub.utils import make_chunks



def audio_chunks_from_file_generator(filename):
  #this simulates the streaming microphone using an input file

  myaudio = AudioSegment.from_file(filename , "wav")
  chunk_length_ms = 100 # pydub calculates in millisec
  chunks = make_chunks(myaudio, chunk_length_ms) #Make chunks of one sec

  for i, chunk in enumerate(chunks):
          #print("chunk*")
          time.sleep(0.1)
          yield chunk.raw_data

cli = k.ILSPASRClient(host="dialogue3.ilsp.gr", port=1234)

for partial_result in cli.streaming_recognize(audio_chunks_from_file_generator(sys.argv[1]):
    # Print best path partial transcription
    print(partial_result.results[0].alternatives[0].transcript)
