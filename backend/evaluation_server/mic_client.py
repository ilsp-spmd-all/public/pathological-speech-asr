from __future__ import division
import datetime
import re
import sys
import time
import websocket
import pyaudio
from six.moves import queue
from websocket import create_connection
import time
from multiprocessing import Process, Queue
import logging
# Audio recording parameters
RATE = 44100
CHUNK = int(RATE / 10)  # 100ms
PORT = 8888
ENDPOINT = "ws://localhost:" + str(PORT) + "/ws-streaming-linux"
#ENDPOINT = "ws://dialogue.ilsp.gr:" + str(PORT) + "/ws-streaming-linux"

''' This script records audio from the microphone (using a Pyaudio
    recording interface) and  streams it to google for transcribing.
    
    Here we instantiate an ASR client. Then main() calls method run().
    In essence, this method activates the client. It spawns two processes.
    One (run_record_and_send() ) is for sending the audio chunk by chunk between 100ms intervals.
    The other (run_receive_and_print) is for handling the incoming responses from the asr server.

    Arguments:
     
     None

    '''


class MicrophoneStream(object):
    """Opens a recording stream as a generator yielding the audio chunks."""

    def __init__(self, rate, chunk):
        self._rate = rate
        self._chunk = chunk

        # Create a thread-safe buffer of audio data
        self._buff = queue.Queue()
        self.closed = True

    def __enter__(self):
        self._audio_interface = pyaudio.PyAudio()
        self._audio_stream = self._audio_interface.open(
            format=pyaudio.paInt16,
            # The API currently only supports 1-channel (mono) audio
            # https://goo.gl/z757pE
            channels=1, rate=self._rate,
            input=True, frames_per_buffer=self._chunk,
            # Run the audio stream asynchronously to fill the buffer object.
            # This is necessary so that the input device's buffer doesn't
            # overflow while the calling thread makes network requests, etc.
            stream_callback=self._fill_buffer,
        )

        self.closed = False

        return self

    def __exit__(self, type, value, traceback):
        self._audio_stream.stop_stream()
        self._audio_stream.close()
        self.closed = True
        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.
        self._buff.put(None)
        self._audio_interface.terminate()

    def _fill_buffer(self, in_data, frame_count, time_info, status_flags):
        """Continuously collect data from the audio stream, into the buffer."""
        self._buff.put(in_data)
        return None, pyaudio.paContinue

    def generator(self):
        while not self.closed:
            # Use a blocking get() to ensure there's at least one chunk of
            # data, and stop iteration if the chunk is None, indicating the
            # end of the audio stream.
            chunk = self._buff.get()
            if chunk is None:
                return
            data = [chunk]

            # Now consume whatever other data's still buffered.
            while True:
                try:
                    chunk = self._buff.get(block=False)
                    if chunk is None:
                        return
                    data.append(chunk)
                except queue.Empty:
                    break

            yield b''.join(data)


class ASRClient(object):

    def __init__(self):
        self.isClosed = False
        self.ws = create_connection(ENDPOINT)

    def incoming_stream_generator(self):
        while (self.isClosed == False):
            result = self.ws.recv()
            yield result

    def run(self):
        # main function
        p1 = Process(target=self.run_record_and_send)
        p2 = Process(target=self.run_receive_and_print)
        p1.start()
        p2.start()
        p2.join()
        p1.terminate()

    def run_record_and_send(self):
        """Handles the audio chunks coming from the microphone stream.

        This method takes the audio chunks created by PyAudio's microphone
        interface and forwards them to the server, using a generator.
        """

        self.ws.send(b"RESTART_COMMUNICATION")
        # send the audio chunk by chunk in 100ms intervals.
        with MicrophoneStream(RATE, CHUNK) as stream:
            logging.info("Using generator")
            audio_generator = stream.generator()
            for chunk in audio_generator:
                self.ws.send_binary(chunk)

    def run_receive_and_print(self):
        """Handles the incoming responses from the asr server.

        It takes care of printing the transcript (both interim and final results)
        in a nicely formatted way, making sure that the every interim overwrites 
        the previous one. It also check if the incoming message is 
        "SINGLE_UTTERANCE_END", in which case we stop handling when we see
        the next final result, using the breakit flag.

        """
        before_conv = datetime.datetime.utcnow()
        incoming_stream = self.incoming_stream_generator()
        responses = (x for x in incoming_stream)
        prev_response_length = 0
        breakit = False
        # The following loop processes the incoming response stream (as a result of a generator)
        for response in responses:
            # the response could either be interim (if we have interim enabled)
            # or final. 'Final' means that google has made its final decision
            # upon the transcript, and that it will continue to proceed the next
            # utterance. However, up until now, we are working with single_utterance_mode,
            # which means that we only the first utterance will be transcribed.

            if response == "SINGLE_UTTERANCE_END":
                # This message is sent by the server when it has detected
                # the end of the first utterance. Unfortunately, it is also
                # the last that is transcribed, as Google actually
                # doesn't provide this signal if we ask it to
                # keep transcribing after the first utterance.
                # Now we will set a flag to True, meaning that
                # as soon as we get the next (also the first) final
                # result we will
                # stop awaiting for responses.

                breakit = True
                continue

            # else, it is either an Interim or a Final result.
            # to overwrite the previous one
            overwrite_chars = ' ' * (prev_response_length-len(response))

            # Interim results are prefixed with _INTERIM by the server.
            substring = response[:8]
            if substring == "_INTERIM":
                    # we print the result and we return to the beginning of the line.
                    # we do this because we want the following result to overwrite the
                    # previous output. This is how we achieve this smooth printing of
                    # the interims and, in the end, the final result"
                sys.stdout.write(response+overwrite_chars + '\r')
                sys.stdout.flush()
                prev_response_length = len(response)
            elif substring[:4] == "_MP3":
                print(len(response))
                print("Got response")
                time.sleep(1)
                print("END OF ROUND")
                print("END OF ROUND")
                break

            elif substring != "_INTERIM":
                print(datetime.datetime.utcnow()-before_conv)
                print(response+overwrite_chars)
                print("got final response, now waiting")

                if re.search(r'\b(τερματισμός|stop)\b', response, re.I):
                    #print('DEBUG: Exiting..')
                    break


def main():
    newClient = ASRClient()
    logging.basicConfig(format='%(asctime)s - %(levelname)s : %(message)s')
    time.sleep(1)
    print("Ξεκινήστε να μιλάτε")
    for i in range(2):
        print("turn" + str(i))
        newClient.run()


if __name__ == '__main__':
    main()
