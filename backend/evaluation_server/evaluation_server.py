import asyncio
import aiohttp
import re
import json
from concurrent.futures import ThreadPoolExecutor
from websockets.client import WebSocketClientProtocol
from typing import Optional,List
from fastapi import FastAPI, WebSocket
#from loguru import logger
from starlette.websockets import WebSocketDisconnect
from websockets.exceptions import ConnectionClosed
import websockets
from pydantic import BaseModel
import traceback
import constants
from Levenshtein import distance as lev
from jiwer import wer

from functools import wraps
from time import time
# TEMP: remove this

def timing(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time()
        result = f(*args, **kw)
        te = time()
        print('func:%r args:[%r, %r] took: %2.4f sec' % (f.__name__, args, kw, te-ts))
        return result
    return wrap


app = FastAPI(
            title="Plan-V speech evaluation backend",
            description='Websocket server for evaluation of speech input of language production exercises. \n\n  * Use the `/audio-evaluation-ws` endpoint.\n* Messages are (utf-8) **text serialized json dicts**.\n * Open **one websocket per recording**\n*  First message has `"is_start": true`, and must declare the current exercise and question, using `exercise_id` and `q_id` fields.\n* The rest messages contain JSONs with **base64 encoded 16bit 16kHz WAV** chunks.\n* Exact JSON schemas available at swagger of `/audio-evaluation-ws-message-format`',
        )

RECORD = False
# ASR_URL="ws://localhost:1337/asr-b64-chunks"
ASR_URL="ws://asr:1337/asr-b64-chunks"
DATABASE_UPDATE_URL = 'https://api.planv.dock1.pikei.io/api/v1/audioUpload'

# reference here
# https://stackoverflow.com/questions/59073556/how-to-cancel-all-remaining-tasks-in-gather-if-one-fails
class TranscriptionComplete(Exception):
    pass

class ClientWebsocketMessageModel(BaseModel):
    target: str
    target_sentence: str
    is_start: bool
    is_chunk: bool
    base64_chunk: str
    is_end_of_stream: bool
    user_id: str
    filename: str
    pageNum: int
    plan_temp_id: int
    item_id: int


class WebsocketResponse(BaseModel):
    status: int
    score: int
    is_error: bool
    is_single_utterance_end: bool
    error_message: str
    correctness_threshold: int
    recognition_result: str

class UpdateExternalDBModel(BaseModel):
    filename: str
    pageNum: int
    plan_temp_id: int
    item_id: int
    user_id: str
    score: int
    text: str
    
    

async def read_client_websocket(ws):
    json_text = await ws.receive_text()
    try:
        requestObject = ClientWebsocketMessageModel.parse_obj(json.loads(json_text))
        
        
        if requestObject.is_chunk:
            assert not(requestObject.is_start)
            assert requestObject.base64_chunk !=""
        if requestObject.is_start:
            print("is_start")
            # print(requestObject)
            assert not(requestObject.is_chunk)
            if requestObject.target=="":
                assert requestObject.target_sentence!=""
            else:
                assert requestObject.target_sentence==""
            assert requestObject.filename!=""
            assert requestObject.pageNum
            assert requestObject.plan_temp_id
            assert requestObject.item_id
            assert requestObject.user_id!=""
            
#             "filename": "rec_2",
# "pageNum": 2,
# "plan_temp_id": 18,
# "item_id": 4,
# "user_id": "p-uuid"
            # assert requestObject.target!="" 
            print("assertion complete")
        if requestObject.is_end_of_stream:
            assert not(requestObject.is_chunk)
            assert not(requestObject.is_start)
            assert requestObject.base64_chunk ==""
            print("assertion completee")
    
    except AssertionError as e:
        print("errorrr")
        print("Got badly formatted json")
        return None

    return requestObject

@app.post('/audio-evaluation-ws-message-format/', response_model=WebsocketResponse)
async def post(msg : ClientWebsocketMessageModel):

    response = WebsocketResponse(status=0,score=0,is_error=False,is_single_utterance_end=False,error_message="", correctness_threshold=0,recognition_result="")
    return response

# @app.post('https://api.planv.dock1.pikei.io/api/v1/audioUpload', response_model=UpdateExternalDBModel)
# async def post_to_db(msg : ClientWebsocketMessageModel):

#     response = WebsocketResponse(status=0,score=0,is_error=False,is_single_utterance_end=False,error_message="", correctness_threshold=0,recognition_result="")
#     return response

class Evaluation:

    def __init__(
        self,
        ws: WebSocket,
        asr_websocket: WebSocketClientProtocol,
    ):
        self.in_ws = ws
        self.asr_websocket = asr_websocket
        self.tasks = {}
        self.target=""
        self.target_sentence=""
        # is sentence is determined whether the start message contains a non-empty target_sentence or target field
        self.is_sentence=True
        self.update_db_after= True
        self.user_id=""
        self.filename=""
        self.pageNum=0
        self.plan_temp_id=0
        self.item_id=0

    def evaluate_phrase(self,transcription):
        trans=transcription.lower()
        trans=re.sub("<unk>","",trans)
        targ=self.target.lower()
        dist = lev(trans,targ)
        if dist==0:
            return 100
        elif dist<4:
            return 100-(dist*15)
        else:
            return 0
        
    def evaluate_sentence(self,transcription):
        #return the word error rate between the target and the transcription
        print("target sentence: ",self.target_sentence)
        print("transcription: ",transcription)
        wer_score=(1-wer(self.target_sentence,transcription))*100
        print("sentence similarity score: ",wer_score)
        return wer_score

    def generate_evaluation_response(self,transcription,is_sentence):
        
        if is_sentence:
            eval_score=self.evaluate_sentence(transcription)
        else:
            eval_score=self.evaluate_phrase(transcription)
        response = WebsocketResponse(status=0,score=eval_score,is_error=False,is_single_utterance_end=False,error_message="", correctness_threshold=50,recognition_result=transcription)

        return response 
    
    # async def update_external_db(self,transcription):
        
    async def update_external_db(self,db_update_url,statistics):
        # url = 'https://api.planv.dock1.pikei.io/api/v1/audioUpload'
        # statistics = {
        #     "filename": "rec_2",
        #     "pageNum": 2,
        #     "plan_temp_id": 18,
        #     "item_id": 4,
        #     "user_id": "p-uuid",
        #     "score": 60,
        #     "text": "NO_RESULT2"
        # }
        print(statistics)
        print(type(statistics))
        data = aiohttp.FormData()
        data.add_field('statistics', json.dumps(statistics), content_type='application/json')

        async with aiohttp.ClientSession() as session:
            async with session.post(db_update_url, data=data) as response:
                response_text=await response.text()
                print("db response text: ",response_text)
                return response_text


    async def receive_and_forward_chunks(self):
        try:
            while True:

                requestObject = await read_client_websocket(self.in_ws)

                if requestObject is None:
                    print("error")
                    response = WebsocketResponse(status=0,score=0,is_error=True,is_single_utterance_end=False,error_message="Badly formatted json", correctness_threshold=0,recognition_result="")
                    await self.in_ws.send_text(json.dumps(vars(response)))
                    break
                elif requestObject.is_start:
                    print("Got start message")
                    if requestObject.target=="":
                        print("setting target sentence")
                        self.target_sentence=requestObject.target_sentence
                        self.is_sentence=True
                        self.update_db_after=True
                    else:
                        self.target=requestObject.target
                        self.is_sentence=False
                        self.update_db_after=False
                    # self.target=requestObject.target
                    self.user_id=requestObject.user_id
                    self.filename=requestObject.filename
                    self.pageNum=requestObject.pageNum
                    self.plan_temp_id=requestObject.plan_temp_id
                    self.item_id=requestObject.item_id

                    print("Sending restart com")
                    await self.asr_websocket.send(constants.RESTART_COMMUNICATION)
                elif requestObject.is_end_of_stream:
                    print("Sending end of stream")
                    await self.asr_websocket.send(constants.END_OF_STREAM)
                    #TODO: send dummy score and transcription to external db
                    
                    break
                else:
                    await self.asr_websocket.send(requestObject.base64_chunk)
             
        except WebSocketDisconnect:
            print("Warning: client closed (Caught on receiveloop)")
            raise WebSocketDisconnect
        except ConnectionClosed as e:
            print("Warning: asr closed (Caught on receiveloop)")
            raise ConnectionClosed(e.code,e.reason)
        except asyncio.CancelledError:
            print("receive_and_forward chunk task cancelled")
        
        print("breaking stream task")

    async def evaluate_transcript(self,is_sentence,update_db_after):
        try: 
            while True:
                textmsg = await self.asr_websocket.recv()
                print(textmsg)
                if "_ERROR_ASR_TAKING_TOO_LONG" in textmsg:
                    print("ASR taking too long")
                    responseObject = WebsocketResponse(status=0,score=0,is_error=True,is_single_utterance_end=False,error_message="ASR taking too long to respond", correctness_threshold=0,recognition_result="")
                    await self.in_ws.send_text(json.dumps(vars(responseObject)))
                    raise TranscriptionComplete
                elif ("INTERIM" not in textmsg) and (constants.END_OF_TRANSCRIPTION not in textmsg) and not update_db_after:
                    print("we got the final message, use it to evaluate")
                    responseObject = self.generate_evaluation_response(textmsg,is_sentence)
                    await self.in_ws.send_text(json.dumps(vars(responseObject)))
                    raise TranscriptionComplete
                elif ("DUMMY" not in textmsg) and (constants.END_OF_TRANSCRIPTION not in textmsg) and update_db_after:
                    print("we got the final message, use it to evaluate and then update external db")
                    responseObject = self.generate_evaluation_response(textmsg,is_sentence)
                    # self.update_external_db(json.dumps(vars(responseObject)))
                    await self.update_external_db(DATABASE_UPDATE_URL,vars(responseObject))
                    # await self.in_ws.send_text(json.dumps(vars(responseObject)))
                    raise TranscriptionComplete
                elif ("DUMMY" in textmsg):
                    print("Send dummy result to pikei API")
                    responseObject = self.generate_evaluation_response(textmsg,is_sentence)
                    #TODO:uncomment this to sent to pikei
                    await self.in_ws.send_text(json.dumps(vars(responseObject)))
                    print("dummy result message: ",responseObject)
                    await self.in_ws.close()

                    # raise TranscriptionComplete
                
                #elif ("INTERIM" in textmsg):
                #    # temporary 
                #    await self.in_ws.send_text(textmsg)
                #TODO organize error codes

        except WebSocketDisconnect:
            print("Warning on evaluation task: client closed (Caught on receiveloop)")
            raise WebSocketDisconnect
        except ConnectionClosed as e:
            print("Warning on evaluation task: asr closed (Caught on receiveloop)")
            raise ConnectionClosed(e.code,e.reason)
        except asyncio.CancelledError:
            print("receive_and_forward chunk task cancelled")
        
        print("breaking evaluation task")


    async def run(self):
        try:
            stream_task= asyncio.create_task(self.receive_and_forward_chunks())
            evaluate_task= asyncio.create_task(self.evaluate_transcript(is_sentence=self.is_sentence,update_db_after=self.update_db_after))
            print(type(stream_task))
            print(type(evaluate_task))
            await asyncio.gather(stream_task,evaluate_task)

        except TranscriptionComplete:
            print("Evaluation complete, closing tasks")
            stream_task.cancel()
            # print("Evaluation complete, closing tasks")
            # await self.in_ws.close()
            print("Evaluation complete, closing tasks")
            await self.asr_websocket.wait_closed()

        except WebSocketDisconnect:
            print("Websocket disconnected")
            stream_task.cancel()
            print("Websocket disconnected")
            evaluate_task.cancel()
            # client websocket closed, now to close the asr one
            print("Websocket disconnected")
            await self.asr_websocket.wait_closed()

        except ConnectionClosed:
            stream_task.cancel()
            evaluate_task.cancel()
            # asr websocket closed, now to close the client one
            await self.in_ws.close()
        except Exception as e:
            print(e)

@app.websocket("/audio-evaluation-ws")
async def evaluation(client_websocket: WebSocket):
    await client_websocket.accept()
    print("accepted client ws")
    asr_socket = await websockets.connect(ASR_URL)
    print("connected asr ws")
    ev = Evaluation(client_websocket,asr_socket)
    await ev.run()

# # Example usage
# result = await send_post_request()
# print(result)

