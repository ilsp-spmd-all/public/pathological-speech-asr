from typing import Iterable, List

import pydub
from google.cloud import speech
from pydub.utils import make_chunks

import constants


def chunked_audio_file(fname: str, chunk_size_ms: int = 100, sr: int = 44100):
    audio = pydub.AudioSegment.from_file(fname)

    if sr != audio.frame_rate:
        audio.set_frame_rate(sr)

    for chunk in make_chunks(audio, chunk_size_ms):
        yield chunk.raw_data


class AudioFileStream:
    """Opens a recording stream as a generator yielding the audio chunks."""

    def __init__(self, fname: str, sr: int = 44100, chunk_size_ms: int = 100):
        self._fname = fname
        self._sr = sr
        self._chunk_size = chunk_size_ms

    def __enter__(self):
        self._chunks = chunked_audio_file(
            self._fname, chunk_size_ms=self._chunk_size, sr=self._sr
        )

        return self

    def __exit__(self, type, value, traceback):
        pass
        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.

    def recv(self):
        for chunk in self._chunks:
            yield chunk


class StreamingGoogleASR:
    def __init__(self, sr: int = 44100, lang: str = "el-GR"):
        encoding = speech.RecognitionConfig.AudioEncoding.LINEAR16
        self.client = speech.SpeechClient()
        config = speech.RecognitionConfig(
            encoding=encoding,
            sample_rate_hertz=sr,
            language_code=lang,
        )

        self.streaming_config = speech.StreamingRecognitionConfig(
            config=config,
            interim_results=True,
            single_utterance=constants.SINGLE_UTTERANCE,
        )

    def _best_path_transcript(self, response):
        if not response.results:
            return None

        result = response.results[0]

        if not result.alternatives:
            return None

        if result.stability and result.stability < constants.STABILITY_THRESHOLD:
            return None

        transcript = result.alternatives[0].transcript

        return transcript, result.is_final

    def _responses(self, requests: List[speech.StreamingRecognizeRequest]):
        for response in self.client.streaming_recognize(
            self.streaming_config, requests  # iter(requests)
        ):
            res = self._best_path_transcript(response)

            if res is not None:
                transcription, finished = res
                yield transcription, finished

                if finished:
                    break

    def recognize(self, data_generator: Iterable):
        for result in self._responses(
            speech.StreamingRecognizeRequest(audio_content=audio_chunk)
            for audio_chunk in data_generator
        ):
            yield result

    def recognize_print(self, data_generator: Iterable):
        for resp, finish in self.recognize(data_generator):
            print(resp)

            if finish:
                break


def main():
    import sys

    streaming_asr = StreamingGoogleASR(sr=constants.SAMPLE_RATE, lang="el-GR")

    with AudioFileStream(
        sys.argv[1], sr=constants.SAMPLE_RATE, chunk_size_ms=100
    ) as stream:
        streaming_asr.recognize_print(stream.recv())


if __name__ == "__main__":
    main()
