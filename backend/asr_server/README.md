# Reimplementation of ASR websocket server


The overall system consists of two websockets, `audio_bridge` and `asr_server`.

Client communicates with the `audio_bridge` and then the data are forwarded to the `asr_server`


```
 [client] <- WS -> [audio bridge] <- WS -> [asr server] <- GRPC -> [Google API]
```

The `audio_bridge` can be used as a central hub that implements the master service.


To run:

```
export GOOGLE_APPLICATION_CREDENTIALS=/path/to/google.json
pip install -r requirements.txt
uvicorn --port 8000 audio_ws_bridge:app
uvicorn --port 8001 asr_server:app
python client.py path/to/test.wav  # 44100 Hz, single channel
```



# kspeech

This module is a client for the kaldi-grpc server. 

Soon --> this will be used to make an `asr_server` that uses kaldi instead of Google's ASR. 
An important detail:

```
Kaldi doesn't stop recognizing at the end of the first utterance;
we need to add the end of utterance at the asr_server.
```
This can be achieved by catching the case in which no result is sent for the last 3 seconds.

