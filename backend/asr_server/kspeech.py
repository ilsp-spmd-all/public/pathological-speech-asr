import os
import sys
import time
from iterators import TimeoutIterator
import kaldigrpc_client as k
from pydub import AudioSegment
from pydub.utils import make_chunks
KALDI_FINISH_TIMEOUT=int(os.environ.get("KALDI_FINISH_TIMEOUT","12"))
KALDI_HOST=os.environ.get("KALDI_HOST","compute.ilsp.gr")
KALDI_PORT=int(os.environ.get("KALDI_PORT",1312))

def audio_chunks_from_file_generator(filename):
  #this simulates the streaming microphone using an input file

  myaudio = AudioSegment.from_file(filename , "wav")
  chunk_length_ms = 100 # pydub calculates in millisec
  chunks = make_chunks(myaudio, chunk_length_ms) #Make chunks of one sec

  for i, chunk in enumerate(chunks):
          #print("chunk*")
          time.sleep(0.1)
          yield chunk.raw_data

def audio_chunk_generator(audio_chunks):
    while True:
        c=next(audio_chunks)
        yield c

def streaming_recognize(audio_chunks):
    cli = k.ILSPASRClient(host=KALDI_HOST,port=KALDI_PORT)
    new_result="NO_RESULT"
    previous_result="NO_RESULT"
    is_final=False
    for partial_result in TimeoutIterator(cli.streaming_recognize(audio_chunk_generator(audio_chunks)),timeout=KALDI_FINISH_TIMEOUT,sentinel=None):   
        if partial_result is None:
            new_result=previous_result
            print("kaldi timeout, returing again: "+new_result)
            is_final=True
            break
        else:
            new_result=partial_result.results[0].alternatives[0].transcript
            if partial_result.results[0].is_final:
                print("got is final flag")
                is_final=True
                break
            if new_result==previous_result and new_result.strip()!="":
                print("got same result, closing")
                is_final=True
                break
            else:
              previous_result=partial_result.results[0].alternatives[0].transcript
              print(new_result)
              is_final=False

        yield new_result,is_final
    yield new_result,is_final

if __name__ =="__main_":
    filename = sys.argv[1]
    for result,is_final in streaming_recognize(audio_chunks_from_file_generator(filename)):
        print(result)
        print(is_final)
