import logging
import os
import sys
from opencensus.ext.azure.log_exporter import AzureLogHandler

import constants

custom_formatter = logging.Formatter('%(asctime)s - %(levelname)s : %(message)s')

class CustomLogger():
    _logger = None

    def __new__(cls):
        if cls._logger is None:
            #        format='%(name)s:: %(asctime)s - %(levelname)s : %(message)s',
            #        filename=os.path.join(os.path.dirname(os.path.realpath(__file__)), "asr_log.log")
            #)

            cls._logger = logging.getLogger(constants.APP_NAME)
            cls._logger.setLevel(logging.DEBUG)
            # propagate messages to root-logger
            # root logger is responsible for storing logsfrom all loggers  in 
            # a file and remotely (through an AzureLogHandle 
            cls._logger.propagate = True
            # set up an StreamHandler for output to console
            # logLevel: INFO
            console_stream_handler = logging.StreamHandler()
            console_stream_handler.setLevel(logging.INFO)
            console_stream_handler.setFormatter(custom_formatter)
            cls._logger.addHandler(console_stream_handler)

        return cls._logger

# util function for adding a file handler and an AzureLogHandler to the root logger
def add_handlers_to_root_logger():

    # get root logger
    root_logger = logging.getLogger()

    # add a FileHandler
    # set up an FileHandler for detailed logging in a file
    # logLevel: DEBUG
    filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), f"{constants.APP_NAME}.log")
    file_stream_handler = logging.FileHandler(filename)
    file_stream_handler.setLevel(logging.DEBUG)
    file_stream_handler.setFormatter(custom_formatter)
    root_logger.addHandler(file_stream_handler)

    # add a AzureLogHandler if it is possible
    # get connection_string from an env-var
    APPLICATIONINSIGHTS_CONNECTION_STRING = os.environ['APPLICATIONINSIGHTS_CONNECTION_STRING'] if "APPLICATIONINSIGHTS_CONNECTION_STRING" in os.environ else None

    # add AzureLogHandler if env-var is set
    # logLevel: INFO
    if APPLICATIONINSIGHTS_CONNECTION_STRING:
        azure_handler = AzureLogHandler()
        azure_handler.setLevel(logging.INFO)
        azure_handler.setFormatter(custom_formatter)

        def _callback_function(envelope):
            envelope.tags['ai.cloud.role'] = constants.APP_NAME

        # add a custom telemetry processor to change role name to
        # https://learn.microsoft.com/en-us/azure/azure-monitor/app/app-map?tabs=python
        azure_handler.add_telemetry_processor(_callback_function)
        root_logger.addHandler(azure_handler)
    else:
        # print this message with custom logger
        logger = CustomLogger()
        logger.info("ApplicationInsights connection string hasn't been set...")

# if it is run by uvicorn ,then we must make sure that
# uvicorn handlers use the same formatter(<-TODO) , and that all messages
# are propagated to the root-logger which is responsible for 
# saving logs in a file, etc
# TODO: modify also format of uvicorn's loggers
def enable_propagation_in_uvicorn():
    for logger_name in ['uvicorn', 'uvicorn.access', 'uvicorn.error']:
        lg = logging.getLogger(logger_name)
        lg.propagate = True


