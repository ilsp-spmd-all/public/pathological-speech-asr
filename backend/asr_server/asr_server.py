import asyncio
from datetime import datetime
from concurrent.futures import ThreadPoolExecutor

import base64
import janus
from fastapi import FastAPI, WebSocket, Response, status
from starlette.websockets import WebSocketDisconnect
from websockets.exceptions import ConnectionClosedError
from iterators import TimeoutIterator
import os
import sys
import logging
import traceback
import constants
from connection_manager import ConnectionManager
from kspeech import streaming_recognize
from gspeech import StreamingGoogleASR
import utils
import uuid

import io
from pydub import AudioSegment
from functools import wraps
from time import time

import grpc
import generated.health_pb2
import generated.health_pb2_grpc

from pywhispercpp.model import Model
import wave
import functools
# get the custom logger (which is used in every module imported here)
# the custom logger is a singleton class: we use it to print stuff to stdout and also propagate to the
# the root logger. The root logger is a singleton class as well: that means that any configuration
# made to the root logger in any one imported module affects the root logger in this file too.

# we use own logger schema:

# rootLogger -> emits to azureLogHandler, filehandler
# ├── uvicorn -> propagates
# │   ├── uvicorn.access -> propagates, emits to stdout
# │   └── uvicorn.error -> propagates, emits to stdout
# └── customLogger -> propagates, emits to stdout

logger = utils.CustomLogger()
# add custom handlers (FileHandler, AzureLogHander, etc) in
# order to get all messages from all non-root loggers
# NOTE: the non-root loggers must have enabled message propagation
utils.add_handlers_to_root_logger()

# if it is run by uvicorn ,then we must make sure that
# uvicorn handlers use the same formatter(<-TODO) , and that all messages
# are propagated to the root-logger which is responsible for 
# saving logs in a file, etc
# TODO: modify also format of uvicorn's loggers
if 'uvicorn' in [os.path.basename(arg) for arg in sys.argv]:
    logger.info('Uvicorn is used.')
    utils.enable_propagation_in_uvicorn()


def timing(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time()
        result = f(*args, **kw)
        te = time()
        logger.info('func:%r args:[%r, %r] took: %2.4f sec' % (f.__name__, args, kw, te-ts))
        return result
    return wrap

app = FastAPI()

conn_manager = ConnectionManager()

RECORD = False
IS_BYTES=False
GOOGLE_DELAY_TOLERANCE=int(os.environ.get("GOOGLE_DELAY_TOLERANCE","17"))
# ASR_BACKEND=os.environ.get("ASR_BACKEND","Kaldi")
# ASR_BACKEND=str(ASR_BACKEND)
ASR_BACKEND="Whisper"
if ASR_BACKEND=="Google":
    logger.info("ASR-Backend: Google")
elif ASR_BACKEND=="Kaldi":
    logger.info("ASR-Backend: Kaldi on-premises")
    # get env vars containing hostname and port of the asr backend
    KALDI_HOST = os.environ.get("KALDI_HOST","localhost")
    KALDI_PORT = int(os.environ.get("KALDI_PORT",1312))
elif ASR_BACKEND=="Whisper":
    logger.info("ASR-Backend: Whisper")
    WHISPER_HOST = os.environ.get("WHISPER_HOST","localhost")
    WHISPER_PORT = int(os.environ.get("WHISPER_PORT",1313))
else:
    logger.error("Failed to find config for asr backend")
    #exit()

class TranscriptionComplete(Exception):
    pass

# reference here
# https://stackoverflow.com/questions/59073556/how-to-cancel-all-remaining-tasks-in-gather-if-one-fails
class ErrorThatShouldCancelOtherTasks(Exception):
    pass

class GoogleTakingTooLong(Exception):
    pass


class ASRWebsocketConnectorWhisper:
    def __init__(
        self,
        ws: WebSocket,
        executor: ThreadPoolExecutor,
    ):
        self.ws = ws
        self.executor = executor
        self.is_bytes= False
        self.transcription=""
        # Same initialization code as before

    async def read_until_declare_start(self):
        print("Reached read_until_declare_start")
        while True:
            if self.is_bytes:
                data = await self.ws.receive_bytes()
                if data == constants.RESTART_COMMUNICATION.encode():
                    break
            else:
                message = await self.ws.receive_text()
                if message == constants.RESTART_COMMUNICATION:
                    logger.info('Got restart msg')
                    break
            logger.warning('Didnt get restart message before stream')
        logger.info('Received restart communication message')

    async def stream_chunks(self):
        logger.info("Starting streaming task...")
        audio_bytes = b""
        try:
            now=time()
            while True:

                await asyncio.sleep(0)

                if self.is_bytes:
                    data = await self.ws.receive_bytes()

                    diff=time()-now
                    now= now+diff
                    logger.debug("After "+ str(diff)+ "s: CHUNK RECEIVED")
                    
                    if data is None:
                        break

                    if data == constants.END_OF_STREAM.encode():
                        break

                    await self.stream_q.async_q.put(data)
                    self.stream_q.async_q.task_done()

                    self.audiobytesacc.append(data)

                else:
                    #TODO:add timeout here
                    data = await self.ws.receive_text()
                    diff=time()-now
                    now= now+diff
                    logger.debug("after "+ str(diff)+ "s: CHUNK RECEIVED")
                    
                    if data is None:
                        break

                    if data == constants.END_OF_STREAM:
                        break

                    # await self.stream_q.async_q.put(base64.b64decode(data))
                    # self.stream_q.async_q.task_done()
                    audio_bytes += base64.b64decode(data)
                    # self.audiobytesacc.append(base64.b64decode(data))
        
        except asyncio.CancelledError:
            logger.info("Stream audio task cancelled")
        except WebSocketDisconnect:
            logger.warning("Client closed (Caught on receiveloop)")
            raise ErrorThatShouldCancelOtherTasks
        except ConnectionClosedError:
            raise ErrorThatShouldCancelOtherTasks
            
        
        logger.info("Breaking stream task...")
        return audio_bytes
    
    def write_audio_file(self, audio_bytes):
        logger.info("Writing audio file...")
        date_time = datetime.now().strftime("%d-%m-%Y_%H%M%S")
        uniquefilename = date_time + "audio"
        file_path = "./captures/" + uniquefilename + ".wav"

        # Audio parameters (modify these based on your specific audio format)
        sample_width = 2       # 16-bit audio
        num_channels = 1       # Mono audio
        sample_rate = 16000    # Sample rate (e.g., 16000 Hz)

        # Create a new WAV file and write the header
        with wave.open(file_path, "wb") as wave_file:
            wave_file.setnchannels(num_channels)
            wave_file.setsampwidth(sample_width)
            wave_file.setframerate(sample_rate)
            wave_file.writeframes(audio_bytes)

        return file_path
    
    def transcribe_audio_whispercpp(audio_file, model, lang="el", task="transcribe"):

        segments = model.transcribe(audio_file, speed_up=True,language=lang)
        text=[]
        for segment in segments:
            text.append(segment.text) 
        text=" ".join(text)
        return text

    def call_asr_service(self, audio_file_path,whisper_model="large"):
        logger.info("Calling ASR service...")
        model=Model(whisper_model,models_dir="./models")
        # transcription=self.transcribe_audio_whispercpp(audio_file_path,model)
        # logger.info("model: "+whisper_model)
        # logger.infp("model: "+model)
        segments = model.transcribe(audio_file_path, speed_up=True,language="el")
        text=[]
        for segment in segments:
            text.append(segment.text) 
        print(text)
        text=" ".join(text)
        self.transcription=text
        # Perform the ASR recognition on the audio file and obtain transcriptions
        # You can use the appropriate libraries or APIs for the ASR service you're using
            

    async def run(self):
        loop = asyncio.get_event_loop()
        # self.tasks = {}
        # self.transcription = "NO TRANS"
        self.audiobytesacc = []

        try:
            # audio_bytes = await self.receive_all_bytes()
            audio_bytes= await self.stream_chunks()
            logger.info("Received audio bytes")
            logger.info("Sending dummy message...")
            await self.ws.send_text("DUMMY")
            audio_file_path = self.write_audio_file(audio_bytes)
            #self.call_asr_service(audio_file_path)
            recognition_task = loop.run_in_executor(
                self.executor,
                functools.partial(self.call_asr_service, audio_file_path=audio_file_path)
            )
            await asyncio.gather(
                recognition_task
            )
            # if os.path.isfile(audio_file_path):
            #     os.remove(audio_file_path)
            print("Transcription: ", self.transcription)
            print("Sending transcription to evaluation server...")
            await self.ws.send_text(constants.END_OF_TRANSCRIPTION)
            await self.ws.send_text(self.transcription)
            # self.finito = True
            logger.info("Breaking send-transcript task...")
            raise TranscriptionComplete
        
            # await self.cleanup_queues()

        except TranscriptionComplete:
            logger.info("Transcription complete. Saving files and closing tasks...")
            try:
                if os.path.isfile(audio_file_path):
                    os.remove(audio_file_path)
            except:
                pass

        except Exception as e:
            logger.error(e)
            traceback.print_exc()
            logger.error("An error occured. Exiting WhisperASR and deleting data...")
            try:
                if os.path.isfile(audio_file_path):
                    os.remove(audio_file_path)
            except:
                pass



class ASRWebsocketConnector:
    def __init__(
        self,
        ws: WebSocket,
        streaming_asr: StreamingGoogleASR,
        executor: ThreadPoolExecutor,
        is_bytes: bool
    ):
        self.ws = ws
        self.queues_closed=False
        self.stream_q = janus.Queue()
        self.transcription_q = janus.Queue()
        self.streaming_asr = streaming_asr
        self.executor = executor
        self.finito = False
        self.is_bytes= is_bytes
        self.uuid=uuid.uuid4()
        self.tasks = {}

 
    async def read_until_declare_start(self):
        
        while True:
            if self.is_bytes:
                data = await self.ws.receive_bytes()
                if data == constants.RESTART_COMMUNICATION.encode():
                    break
            else:
                message = await self.ws.receive_text()
                if message == constants.RESTART_COMMUNICATION:
                    logger.info('Got restart msg')
                    break
            logger.warning('Didnt get restart message before stream')
        logger.info('Received restart communication message')



    async def stream_chunks(self):
        logger.info("Starting streaming task...")
        try:
            now=time()
            while True:

                await asyncio.sleep(0)

                if self.is_bytes:
                    data = await self.ws.receive_bytes()

                    diff=time()-now
                    now= now+diff
                    # logger.debug("After "+ str(diff)+ "s: CHUNK RECEIVED")
                    if data is None:
                        break

                    if data == constants.END_OF_STREAM.encode():
                        break

                    await self.stream_q.async_q.put(data)
                    self.stream_q.async_q.task_done()
                    self.audiobytesacc.append(data)

                else:
                    data = await self.ws.receive_text()
                    diff=time()-now
                    now= now+diff
                    # logger.debug("after "+ str(diff)+ "s: CHUNK RECEIVED")

                    if data is None:
                        break

                    if data == constants.END_OF_STREAM:
                        break

                    await self.stream_q.async_q.put(base64.b64decode(data))
                    self.stream_q.async_q.task_done()
                    self.audiobytesacc.append(base64.b64decode(data))
                
        except asyncio.CancelledError:
            logger.info("Stream audio task cancelled")
        except WebSocketDisconnect:
            logger.warning("Client closed (Caught on receiveloop)")
            raise ErrorThatShouldCancelOtherTasks
        except ConnectionClosedError:
            raise ErrorThatShouldCancelOtherTasks
            
        
        logger.info("Breaking stream task...")

    #@timing
    def read_audio(self):
        while not self.finito:
            yield self.stream_q.sync_q.get()

    @timing
    def recognize_blocking(self):
        self.capture=False
        logger.info("Starting blocking recognition task...")
        try:
            if not self.finito:

                if ASR_BACKEND=="Google":
                    ri=TimeoutIterator(self.streaming_asr.recognize(self.read_audio()),timeout=GOOGLE_DELAY_TOLERANCE,sentinel=(None,None))
                else:
                    ri=TimeoutIterator(streaming_recognize(self.read_audio()),timeout=GOOGLE_DELAY_TOLERANCE,sentinel=(None,None))

                for transcription, finished in ri:
                    if transcription is None:
                        logger.warning("Google speech taking too long.")
                        raise GoogleTakingTooLong

                    logger.info("Got transc. : " + transcription)
                    self.transcription_q.sync_q.put((transcription, finished))

                    if finished:
                        while not self.stream_q.sync_q.empty():
                            # added to stop read audio generator
                            if ASR_BACKEND=="Kaldi":
                                self.finito=True
                            self.stream_q.sync_q.get()
                        logger.info("Breaking recognition task")
                        break
        except asyncio.CancelledError:
            logger.info("Recognize blocking cancelled.")
        except GoogleTakingTooLong:
            raise GoogleTakingTooLong
        except Exception as e:
            logger.error(str(e))
            raise ErrorThatShouldCancelOtherTasks

    async def send_transcription(self):
        logger.info("Starting task that sends back transcription...")
        try:
            while True:
                transcription, finished = await self.transcription_q.async_q.get()
                # Correction: why is this not done from here already
                self.transcription_q.async_q.task_done()
                logger.info("Sending transcription: " + transcription)
                await self.ws.send_text("_INTERIM:" + transcription)

                if finished:
                    await self.ws.send_text(constants.END_OF_TRANSCRIPTION)
                    await self.ws.send_text(transcription)
                    self.transcription=transcription
                    self.finito = True
                    logger.info("Breaking send-transcript task...")
                    raise TranscriptionComplete

        except asyncio.CancelledError:
            logger.info("Send-transcript task cancelled.")

        except WebSocketDisconnect:
            logger.warning("Client closed (Caught on sendloop).")
            raise ErrorThatShouldCancelOtherTasks
        except ConnectionClosedError:
            raise ErrorThatShouldCancelOtherTasks

    async def cleanup_queues(self):
        if not(self.queues_closed):
            self.stream_q.close()
            self.transcription_q.close()
            await self.stream_q.wait_closed()
            await self.transcription_q.wait_closed()
            self.queues_closed=True


    async def run(self):
        loop = asyncio.get_event_loop()
        self.tasks={}
        self.transcription="NO TRANS"
        self.audiobytesacc=[]
        try:

            stream_task = asyncio.create_task(self.stream_chunks())
            recognition_task = loop.run_in_executor(
                self.executor,
                self.recognize_blocking,
            )
            transcription_task = asyncio.create_task(self.send_transcription())


            self.tasks = {
                "stream": stream_task,
                "recognize": recognition_task,
                "transcribe": transcription_task,
            }

            await asyncio.gather(
                recognition_task, transcription_task
            )
            stream_task.cancel()
            await self.cleanup_queues()

        except TranscriptionComplete:
            # save the file
            logger.info("Transcription complete. Saving files and closing tasks...")
            stream_task.cancel()
            # might be already stopped, but doesn't hurt to cancel it
            recognition_task.cancel()
            if self.audiobytesacc is not None:
                date_time = datetime.now().strftime("%d-%m-%Y_%H%M%S")
                uniquefilename=date_time+str(self.uuid)
                audio_bytes=b"".join(self.audiobytesacc)
                audio = AudioSegment.from_raw(io.BytesIO(audio_bytes), sample_width=2, frame_rate=16000, channels=1).export('./captures/'+uniquefilename+".wav", format='wav')
                with open('./captures/'+uniquefilename+'.txt',"w") as f:
                    f.write("Transcript: " + self.transcription)
                    f.write("\n")
            await self.cleanup_queues()
        except GoogleTakingTooLong:
            await self.ws.send_text("_ERROR_GOOGLE_TAKING_TOO_LONG")
            for task_n,task in self.tasks.items():
                task.cancel()
            await self.cleanup_queues()


        except ErrorThatShouldCancelOtherTasks:
            for task_n,task in self.tasks.items():
                task.cancel()
            await self.cleanup_queues()

@app.websocket("/asr-b64-chunks")
async def speech_to_text(asr_client: WebSocket):
    executor = ThreadPoolExecutor(max_workers=constants.MAX_WORKERS)
    if ASR_BACKEND=="Google":
        logger.info("ASR-Backend: Google ASR")
        streaming = StreamingGoogleASR(sr=constants.SAMPLE_RATE, lang=constants.LANG)
        asr = ASRWebsocketConnector(
            asr_client,
            streaming,
            executor,
            IS_BYTES
        )
    
    elif ASR_BACKEND=="Kaldi":
        logger.info("ASR-Backend: Kaldi on-premises")
        streaming = None
        asr = ASRWebsocketConnector(
            asr_client,
            streaming,
            executor,
            IS_BYTES
        )
    
    elif ASR_BACKEND=="Whisper":
        logger.info("ASR-Backend: Whisper ASR")
        streaming = None
        asr = ASRWebsocketConnectorWhisper(
            ws=asr_client,
            executor=executor
        )

    
    await conn_manager.connect(
        asr_client,
        asr
    )

    await asr.read_until_declare_start()

    try:

        await asr.run()
        asr_done_running=True
        asr = await conn_manager.disconnect(asr_client)
        logger.info("Client disconnected.")

    except WebSocketDisconnect:
        if not asr_done_running:
            asr = await conn_manager.disconnect(asr_client)
            logger.info("Client disconnected.")


# FastAPI endpoint for health-checking (for liveness) the FastAPI server and the ASR backend server
@app.get("/health",status_code=200)
async def health_check(response: Response):
    # perform the health check by sending a request to the gRPC server
    # and checking the response
    try:
        # connect to the gRPC server
        async with grpc.aio.insecure_channel(f"{KALDI_HOST}:{KALDI_PORT}") as channel:
            stub = generated.health_pb2_grpc.HealthStub(channel)
            print("-------------- Performing health check on grpc server --------------")
            response = await stub.Check(generated.health_pb2.HealthCheckRequest())

            if response.status == generated.health_pb2.HealthCheckResponse.SERVING:
                return "OK!"
            else:
                response.status_code= status.HTTP_503_SERVICE_UNAVAILABLE
                return "NOT OK"
    except grpc.aio.AioRpcError as e:
        # return an error if there was a problem connecting to or communicating with the gRPC server
        response.status_code= status.HTTP_503_SERVICE_UNAVAILABLE
        return "NOT OK"

@app.get("/readiness",status_code=200)
async def readiness_check(response: Response):
    # perform the health check by sending a request to the gRPC server
    # and checking the response
    try:
        # connect to the gRPC server
        async with grpc.aio.insecure_channel(f"{KALDI_HOST}:{KALDI_PORT}") as channel:
            stub = generated.health_pb2_grpc.HealthStub(channel)
            print("-------------- Performing health check on grpc server --------------")
            response = await stub.Check(generated.health_pb2.HealthCheckRequest())

            if response.status == generated.health_pb2.HealthCheckResponse.SERVING:
                return "OK!"
            else:
                response.status_code= status.HTTP_503_SERVICE_UNAVAILABLE
                return "NOT OK"
    except grpc.aio.AioRpcError as e:
        # return an error if there was a problem connecting to or communicating with the gRPC server
        response.status_code= status.HTTP_503_SERVICE_UNAVAILABLE
        return "NOT OK"

